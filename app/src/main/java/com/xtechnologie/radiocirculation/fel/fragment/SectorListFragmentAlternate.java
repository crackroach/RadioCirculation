package com.xtechnologie.radiocirculation.fel.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.fel.activity.AddJourneyDialogActivity;
import com.xtechnologie.radiocirculation.fel.adapter.RouteArrayAdapter;
import com.xtechnologie.radiocirculation.fel.adapter.SectorArrayAdapter;
import com.xtechnologie.radiocirculation.fel.adapter.SimpleJourneyAdapter;
import com.xtechnologie.radiocirculation.fel.dialog.LoadingDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SectorListFragmentAlternate#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SectorListFragmentAlternate extends Fragment implements IDataServiceCaller
{

    private ListView sectorListview, routeListView;
    private SectorArrayAdapter sectorAdapter;
    private RouteArrayAdapter routeAdapter;
    private ViewSwitcher switcher;
    private LinearLayout backButton;
    private TextView sectorTitleRouteSection;
    private RouteLoader routeLoader;
    private SectorLoader sectorLoader;
    private LoadingDialog loadingDialog;
    private int lastLoadedSector;
    private FloatingActionButton fab;
    private List<Route> selectedRoutes;

    private Animation slideRight, slideLeft, slideLeftBack, slideRightBack;

    public SectorListFragmentAlternate()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SectorListFragmentAlternate.
     */
    // TODO: Rename and change types and number of parameters
    public static SectorListFragmentAlternate newInstance()
    {
        SectorListFragmentAlternate fragment = new SectorListFragmentAlternate();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        lastLoadedSector = -1;
        selectedRoutes = new ArrayList<>();
        setRetainInstance(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.fragment_sector_list_fragment_alternate, container, false);
        switcher = (ViewSwitcher) view.findViewById(R.id.sector_switcher_alternate);
        backButton = (LinearLayout) view.findViewById(R.id.sector_back_button_alternate);
        sectorTitleRouteSection = (TextView) view.findViewById(R.id.sector_sector_alternate);
        fab = (FloatingActionButton) view.findViewById(R.id.sector_list_alternate_floating_action_button);
        fab.setVisibility(View.GONE);

        routeLoader = new RouteLoader();
        sectorLoader = new SectorLoader();

        slideRight = AnimationUtils.loadAnimation(getContext(), R.anim.right_to_left_slide);
        slideLeft = AnimationUtils.loadAnimation(getContext(), R.anim.left_to_right_slide);
        slideRightBack = AnimationUtils.loadAnimation(getContext(), R.anim.right_to_left_slide_back);
        slideLeftBack = AnimationUtils.loadAnimation(getContext(), R.anim.left_to_right_slide_back);

        sectorListview = (ListView) view.findViewById(R.id.sector_list_alternate);
        routeListView = (ListView) view.findViewById(R.id.sector_fragment_alternate_route_list);

        sectorListview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Sector sector = (Sector) parent.getItemAtPosition(position);
                sectorTitleRouteSection.setText(sector.getName());
                if(lastLoadedSector != position)
                {
                    lastLoadedSector = position;
                    Services.ROUTE_SERVICE.getRoutesBySector(routeLoader, sector.getId());
                }
                switcher.setOutAnimation(slideRight);
                switcher.setInAnimation(slideLeftBack);
                switcher.showNext();
                fab.setVisibility(View.VISIBLE);
            }
        });

        routeListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Route route = routeAdapter.getItem(position);
                selectedRoutes.add(route);

                view.setActivated(!view.isActivated());

                View childView = view.findViewById(R.id.route_fragment_check_mark);
                int showDelay = 500;

                if(view.isActivated())
                    childView
                        .animate()
                        .alpha(1.0f)
                        .setDuration(showDelay);
                else
                    childView
                        .animate()
                        .alpha(0.0f)
                        .setDuration(showDelay);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                switcher.setInAnimation(slideRightBack);
                switcher.setOutAnimation(slideLeft);
                switcher.showNext();
                uncheckAll();
                fab.setVisibility(View.GONE);
            }
        });

        sectorAdapter = new SectorArrayAdapter(getContext(),new ArrayList<Sector>());
        routeAdapter = new RouteArrayAdapter(getContext(), new ArrayList<Route>());
        sectorListview.setAdapter(sectorAdapter);
        routeListView.setAdapter(routeAdapter);

        setFloatingActionButtonListener();

        Services.ROUTE_SERVICE.fetchAllSectors(sectorLoader);
        return view;
    }

    private void uncheckAll()
    {
        selectedRoutes.removeAll(selectedRoutes);
        for(int i = 0; i < routeListView.getCount(); i++)
        {
            View view = routeListView.getChildAt(i);
            if(view != null)
            {
                view.setActivated(false);
                view.findViewById(R.id.route_fragment_check_mark).setAlpha(0.0f);
            }
        }
    }

    private void setFloatingActionButtonListener()
    {
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO set string
                Services.JOURNEY_SERVICE.getJourneys(new IDataServiceCaller()
                {
                    @Override
                    public void onTaskCompleted(WebServiceResult result)
                    {
                        loadingDialog.dismiss();
                        boolean hasChoices = true;
                        final SimpleJourneyAdapter adapter = new SimpleJourneyAdapter(SectorListFragmentAlternate.this.getContext(), new ArrayList<Journey>());
                        if(result.isSuccess() && !result.getResult(List.class).isEmpty())
                        {
                            adapter.addAll(result.getResult(List.class));
                        }
                        else
                        {
                            hasChoices = false;
                            adapter.add(new Journey("Ajouter un trajet"));
                        }

                        AlertDialog dialog;
                        AlertDialog.Builder builder = new AlertDialog.Builder(SectorListFragmentAlternate.this.getContext());
                        builder.setTitle("Trajet")
                                .setPositiveButton("Créer un trajet", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        startActivity(new Intent(SectorListFragmentAlternate.this.getContext(), AddJourneyDialogActivity.class));
                                    }
                                })
                                .setNegativeButton("Annuler", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                    }
                                });
                        if (hasChoices)
                            builder.setSingleChoiceItems(adapter, 0, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(final DialogInterface dialog, int which)
                                {
                                    Journey journey = adapter.getItem(which);
                                    Services.JOURNEY_SERVICE.addRouteToJourney(new IDataServiceCaller()
                                                                               {
                                                                                   @Override
                                                                                   public void onTaskCompleted(WebServiceResult result)
                                                                                   {
                                                                                       dialog.dismiss();
                                                                                       AlertDialog alertDialog = new AlertDialog.Builder(SectorListFragmentAlternate.this.getContext()).create();
                                                                                       alertDialog.setTitle("Routes ajoutées");
                                                                                       alertDialog.setMessage("Les routes ont bien été ajoutées");
                                                                                       alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                                                               new DialogInterface.OnClickListener() {
                                                                                                   public void onClick(DialogInterface dialog, int which) {
                                                                                                       uncheckAll();
                                                                                                       dialog.dismiss();
                                                                                                   }
                                                                                               });
                                                                                       alertDialog.show();
                                                                                   }

                                                                                   @Override
                                                                                   public void onProgress(Integer... value)
                                                                                   {

                                                                                   }

                                                                                   @Override
                                                                                   public void onPreExecute(Object obj)
                                                                                   {

                                                                                   }

                                                                                   @Override
                                                                                   public void onTaskCancelled(WebServiceResult result)
                                                                                   {

                                                                                   }
                                                                               },
                                            SectorListFragmentAlternate.this.getContext(),
                                            journey.getId(),
                                            selectedRoutes.toArray(new Route[selectedRoutes.size()]));
                                }
                            });
                        else
                            builder.setSingleChoiceItems(adapter, 0, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    startActivity(new Intent(SectorListFragmentAlternate.this.getContext(), AddJourneyDialogActivity.class));
                                }
                            });
                        dialog = builder.show();
                    }

                    @Override
                    public void onProgress(Integer... value)
                    {

                    }

                    @Override
                    public void onPreExecute(Object obj)
                    {
                        showLoadingBar();
                    }

                    @Override
                    public void onTaskCancelled(WebServiceResult result)
                    {
                        loadingDialog.dismiss();
                    }
                }, SectorListFragmentAlternate.this.getContext());
            }
        });
    }

    private void showLoadingBar()
    {
        if(loadingDialog == null)
            loadingDialog = LoadingDialog.newInstance();


        if(!loadingDialog.isShowing())
            loadingDialog.show(getFragmentManager(), LoadingDialog.TAG);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
        lastLoadedSector = -1;
    }

    @Override
    public void onTaskCompleted(WebServiceResult result)
    {
        String TAG = "TEST";
        loadingDialog.dismiss();
        Log.i(TAG, "onTaskCompleted: " + result);
    }

    @Override
    public void onProgress(Integer... value)
    {

    }

    @Override
    public void onPreExecute(Object obj)
    {
        showLoadingBar();
    }

    @Override
    public void onTaskCancelled(WebServiceResult result)
    {
        loadingDialog.dismiss();
    }

    private class SectorLoader implements IDataServiceCaller
    {
        @Override
        public void onTaskCompleted(WebServiceResult result)
        {
            loadingDialog.dismiss();
            sectorAdapter.clear();
            if(result != null && result.isSuccess()){
                List<Sector> sectors = (List) result.getResult();

                sectorAdapter.addAll(sectors);
            }
            sectorAdapter.notifyDataSetChanged();
        }

        @Override
        public void onProgress(Integer... value)
        {

        }

        @Override
        public void onPreExecute(Object obj)
        {
            showLoadingBar();
        }

        @Override
        public void onTaskCancelled(WebServiceResult result)
        {
            loadingDialog.dismiss();
        }
    }

    private class RouteLoader implements IDataServiceCaller
    {
        @Override
        public void onTaskCompleted(WebServiceResult result)
        {
            loadingDialog.dismiss();
            routeAdapter.clear();
            if(result != null && result.isSuccess()){
                Sector sector = result.getResult(Sector.class);

                routeAdapter.addAll(sector);
            }
            routeAdapter.notifyDataSetChanged();
        }

        @Override
        public void onProgress(Integer... value)
        {

        }

        @Override
        public void onPreExecute(Object obj)
        {
            showLoadingBar();
        }

        @Override
        public void onTaskCancelled(WebServiceResult result)
        {
            loadingDialog.dismiss();
        }
    }
}
