package com.xtechnologie.radiocirculation.fel.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;

public class SplashScreen extends AppCompatActivity implements IDataServiceCaller
{

    // private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Stetho.initializeWithDefaults(this);

        setContentView(R.layout.activity_splash_screen);
        //Retirer la barre de titre
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        /**
         * Animation de la planète qui tourne
         */
//        ImageView planet = (ImageView) findViewById(R.id.planet);
//        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
//        rotation.setFillAfter(true);
//        planet.startAnimation(rotation);

        if(!Services.PHONE_SERVICE.isNetworkAvailable(SplashScreen.this))
        {
            Toast.makeText(SplashScreen.this, R.string.app_no_connection_found, Toast.LENGTH_SHORT).show();
        }
//        else if(Services.APP.isFirstRun(SplashScreen.this))
//        {
//            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
//            finish();
//        }
        else if(Services.USER_SERVICE.isLocalUser(this))
        {

            startActivity(new Intent(SplashScreen.this, MainActivity.class));
            finish();
        }
        else if(Services.USER_SERVICE.hasUserTokenStored(this))
        {
            Services.USER_SERVICE.loginWithToken(this);
        }
        else
        {
            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
            finish();
        }


    }

    @Override
    public void onTaskCompleted(WebServiceResult result)
    {
        if(result != null && result.isSuccess())
        {
            startActivity(new Intent(SplashScreen.this, MainActivity.class));
            finish();
        }
        else
        {
            Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT)
                 .show();
        }
    }

    @Override
    public void onProgress(Integer... value)
    {

    }

    @Override
    public void onPreExecute(Object obj)
    {

    }

    @Override
    public void onTaskCancelled(WebServiceResult result)
    {
        Toast.makeText(this, R.string.user_credentials_invalid, Toast.LENGTH_SHORT).show();
    }
}
