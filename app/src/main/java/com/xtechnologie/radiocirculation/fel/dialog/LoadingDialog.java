package com.xtechnologie.radiocirculation.fel.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.xtechnologie.radiocirculation.R;

/**
 * Created by Alexis-Laptop on 2017-04-27.
 */

public class LoadingDialog extends DialogFragment
{
    public static final String TAG = LoadingDialog.class.getSimpleName();
    private static boolean showing = false;
    public static LoadingDialog newInstance()
    {
        return new LoadingDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_loading_dialog, container, false);
        setCancelable(false);
        return view;
    }

    @Override
    public void show(FragmentManager manager, String tag)
    {
        super.show(manager, tag);
        showing = true;
    }

    @Override
    public void dismiss()
    {
        super.dismiss();
        showing = false;
    }

    public static boolean isShowing()
    {
        return showing;
    }
}
