package com.xtechnologie.radiocirculation.fel.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.fel.activity.MainActivity;
import com.xtechnologie.radiocirculation.utils.Preferences;
import com.xtechnologie.radiocirculation.utils.PreferencesKey;

public class NoAccountCreationDialog extends DialogFragment
{
    // L'activité qui va appler le dialog. Cela nous permet de fermer l'activité
    private Activity activity;

    public static NoAccountCreationDialog newInstance(@NonNull Activity activity)
    {
        NoAccountCreationDialog dialog = new NoAccountCreationDialog();
        dialog.setActivity(activity);

        return dialog;
    }

    private void setActivity(Activity activity)
    {
        this.activity = activity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.no_account_creation_dialog_text)
                .setTitle(R.string.no_account_creation_dialog_title)
                .setPositiveButton(R.string.go_on, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Preferences.newInstance(activity.getBaseContext()).setBoolean(PreferencesKey.IS_LOCAL_USER, true);
                        startActivity(new Intent(activity, MainActivity.class));
                        activity.finish();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}