package com.xtechnologie.radiocirculation.fel.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.fel.viewholder.RouteViewHolder;

import java.util.List;

/**
 * Created by Mathieu on 2017-04-16.
 */

public class JourneyArrayAdapter extends ArrayAdapter<Journey> {
    public JourneyArrayAdapter(Context context, List<Journey> journeys) {
        super(context, 0, journeys);
    }




    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {


        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_my_favorite_item,parent, false);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        if(viewHolder == null)
        {
            viewHolder = new ViewHolder();
            viewHolder.favorite_item_txt = (TextView) convertView.findViewById(R.id.favorite_item_txtview);
            viewHolder.favorite_NumberRoutes_txt = (TextView) convertView.findViewById(R.id.numberRoutes_txt);
            viewHolder.favorite_play_button = (ImageView) convertView.findViewById(R.id.favorite_play_button);
            viewHolder.favorite_arrow_txtView = (TextView) convertView.findViewById(R.id.favorite_arrow_txtView);
            convertView.setTag(viewHolder);
        }

        final Journey journey = getItem(position);
        viewHolder.favorite_item_txt.setText(journey.getName());
        String str = getContext().getResources().getString(R.string.number_of_routes_in_favorite);
        viewHolder.favorite_NumberRoutes_txt.setText(str + " " + String.valueOf(journey.getSize()));
        viewHolder.favorite_play_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Services.JOURNEY_SERVICE.getRouteByJourney(new IDataServiceCaller()
                {
                    @Override
                    public void onTaskCompleted(WebServiceResult result)
                    {
                        if(result.isSuccess())
                            Services.AUDIO_MANAGER.play(result.getResult(Journey.class).getRoutes());
                    }

                    @Override
                    public void onProgress(Integer... value)
                    {

                    }

                    @Override
                    public void onPreExecute(Object obj)
                    {

                    }

                    @Override
                    public void onTaskCancelled(WebServiceResult result)
                    {
                        Toast.makeText(getContext(), "Impossible de jouer la piste audio", Toast.LENGTH_SHORT)
                             .show();
                    }
                }, getContext(), journey.getId());
            }
        });

        return convertView;
    }

    public class ViewHolder {
        TextView favorite_item_txt;
        TextView favorite_NumberRoutes_txt;
        ImageView favorite_play_button;
        TextView favorite_arrow_txtView;
    }
}
