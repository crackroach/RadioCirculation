package com.xtechnologie.radiocirculation.fel.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.SettingInjectorService;
import android.media.audiofx.BassBoost;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.web.User;
import com.xtechnologie.radiocirculation.bll.service.Services;

public class SettingsActivity extends AppCompatActivity {

    private TextView email, name, changePwd, disconnect, credits;
    private Switch notification, lockScreenPlayer;
    private boolean isDisconnecting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        email = (TextView) findViewById(R.id.settings_email_adress);
        name = (TextView) findViewById(R.id.settings_name);
        changePwd = (TextView) findViewById(R.id.settings_change_password);
        disconnect = (TextView) findViewById(R.id.settings_disconnect);
        credits = (TextView) findViewById(R.id.settings_credits);
        notification = (Switch) findViewById(R.id.settings_notication_switch);
        lockScreenPlayer = (Switch) findViewById(R.id.settings_lockscreen_player);

        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectDisconnect();
            }
        });

        changePwd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(isDisconnecting)
                {
                    Intent i = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
                    startActivity(i);
                }
            }
        });

        credits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingsActivity.this, CreditsActivity.class);
                startActivity(i);
            }
        });

        lockScreenPlayer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                Services.APP.setNotificationPlayerEnabled(SettingsActivity.this, isChecked);
            }
        });

        User currentUser = Services.USER_SERVICE.getCurrentUser();
        if(currentUser != null)
        {
            email.setText(currentUser.getEmail());
            name.setText(currentUser.getName());
            lockScreenPlayer.setChecked(Services.APP.useNotificationPlayer(this));
            enableAccountSection(true);
        }
        else
            enableAccountSection(false);
    }

    public void enableAccountSection(boolean bool)
    {
        email.setEnabled(bool);
        name.setEnabled(bool);
        changePwd.setEnabled(bool);
        notification.setEnabled(bool);

        isDisconnecting = bool;
        if(bool)
        {
            disconnect.setText(R.string.settings_disconnected);
            disconnect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        else
        {
            disconnect.setText(R.string.settings_connected);
            disconnect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_right_arrow, 0);
        }
    }

    // TODO renommer cette méthode
    private void connectDisconnect()
    {
        if(isDisconnecting) {
            AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
            alert.setMessage(R.string.settings_disconnection_dialog)
                    .setTitle(R.string.settings_disconnection_dialog_title);

            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Services.USER_SERVICE.disconnect(SettingsActivity.this);
                    enableAccountSection(false);
                    startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                    finish();
                }
            });
            alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.create();
            alert.show();
        }
        else
        {
            Intent i = new Intent(SettingsActivity.this, LoginActivity.class);
            i.putExtra(LoginActivity.IS_FROM_SETTINGS, true);
            startActivity(i);
        }
    }
}
