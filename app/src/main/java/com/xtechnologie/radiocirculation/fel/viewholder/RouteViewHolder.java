package com.xtechnologie.radiocirculation.fel.viewholder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Maxime on 2017-04-20.
 */

public class RouteViewHolder
{
    public TextView name;
    public ImageView playButton;
    public TextView sectorName;
}
