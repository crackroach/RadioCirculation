package com.xtechnologie.radiocirculation.fel.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.bll.service.AudioServiceManager;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.fel.viewholder.RouteViewHolder;

import java.io.IOException;
import java.util.List;

/**
 * Created by Maxime on 2017-04-20.
 */

public class RouteArrayAdapter extends ArrayAdapter<Route> {
    private boolean showSectorName;

    public RouteArrayAdapter(Context context, List<Route> routes)
    {
        super(context, 0, routes);
        showSectorName = false;
    }

    public RouteArrayAdapter(Context context, Sector sector)
    {
        super(context, 0, sector.getRoutes());
        showSectorName = false;
    }

    public RouteArrayAdapter(Context context, List<Route> routes, boolean showSectorName)
    {
        super(context, 0, routes);
        this.showSectorName = showSectorName;
    }


    public void addAll(@NonNull Sector sector)
    {
        super.addAll(sector.getRoutes());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        //Si la vue n'existe pas on va la créer
        if(convertView == null)
        {
            // on va chercher quel view va être le réceptacle de nos informations
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_route,parent, false);
        }

        // Un viewHolder est défini dans un autre fichier afin d'accomoder le transfert d'information
        RouteViewHolder viewHolder = (RouteViewHolder) convertView.getTag();

        // si le viewHolder est null on va le créer
        if(viewHolder == null)
        {
            // Nouveau viewHolder
            viewHolder = new RouteViewHolder();

            // on lie chaque champs de notre viewholder à notre vue actuel, le convertView
            viewHolder.name = (TextView) convertView.findViewById(R.id.routeName);
            viewHolder.playButton = (ImageView) convertView.findViewById(R.id.route_fragment_play_button);
            viewHolder.sectorName = (TextView) convertView.findViewById(R.id.route_fragment_sector_title);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Exam> exams
        final Route route = getItem(position);

        // On place chaques informations dans son TextView respectif
        viewHolder.name.setText(route.getName());
        viewHolder.playButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Services.AUDIO_MANAGER.play(route);
                //Services.AUDIO_MANAGER.start();
            }
        });
        viewHolder.sectorName.setText((route.getSector() != null ) ? route.getSector().getName() : "");
        if(showSectorName)
            viewHolder.sectorName.setVisibility(View.VISIBLE);

        // on retourne notre vue pour que la liste l'affiche
        return convertView;
    }
}
