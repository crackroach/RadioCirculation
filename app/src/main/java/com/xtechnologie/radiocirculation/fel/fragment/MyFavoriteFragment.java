package com.xtechnologie.radiocirculation.fel.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.android.gms.vision.text.Text;
import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.service.JourneyService;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.dal.sqlite.JourneyManager;
import com.xtechnologie.radiocirculation.fel.activity.AddJourneyDialogActivity;
import com.xtechnologie.radiocirculation.fel.adapter.JourneyArrayAdapter;
import com.xtechnologie.radiocirculation.fel.adapter.RouteArrayAdapter;
import com.xtechnologie.radiocirculation.fel.dialog.LoadingDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyFavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyFavoriteFragment extends Fragment implements IDataServiceCaller, AddJourneyDialogActivity.AddJourneyDialogOnClose {

    //attributs pour listview(pour test seulement)
    private ListView listViewJourneys;
    private JourneyArrayAdapter adapter;
    private FloatingActionButton addJourneyButton;
    private int position_item;
    private LoadingDialog loadingDialog;
    private ViewSwitcher switcher;
    private LinearLayout backButton;
    private TextView journeyTitleRouteSection;
    private ListView routeListView;
    private int lastLoadedJourney;
    private JourneyArrayAdapter journeyAdapter;
    private RouteArrayAdapter routeAdapter;

    private Animation slideRight, slideLeft, slideLeftBack, slideRightBack;

    public MyFavoriteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MyFavoriteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyFavoriteFragment newInstance() {
        MyFavoriteFragment fragment = new MyFavoriteFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AddJourneyDialogActivity.register(this);
        lastLoadedJourney = -1;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_favorite, container, false);

        //init
        switcher = (ViewSwitcher) view.findViewById(R.id.viewSwitcher_favorite_route);
        backButton = (LinearLayout) view.findViewById(R.id.journey_back_button_alternate);
        journeyTitleRouteSection = (TextView) view.findViewById(R.id.journey_route_alternate);
        listViewJourneys = (ListView) view.findViewById(R.id.myfavorite_journeylistview);
        addJourneyButton = (FloatingActionButton) view.findViewById(R.id.add_journey_button);
        slideRight = AnimationUtils.loadAnimation(getContext(), R.anim.right_to_left_slide);
        slideLeft = AnimationUtils.loadAnimation(getContext(), R.anim.left_to_right_slide);
        slideRightBack = AnimationUtils.loadAnimation(getContext(), R.anim.right_to_left_slide_back);
        slideLeftBack = AnimationUtils.loadAnimation(getContext(), R.anim.left_to_right_slide_back);
        routeListView = (ListView) view.findViewById(R.id.journey_fragment_alternate_route_list);
        journeyAdapter = new JourneyArrayAdapter(view.getContext(), new ArrayList<Journey>());
        routeAdapter = new RouteArrayAdapter(getContext(), new ArrayList<Route>(), true);

        AddJourneyDialogActivity.register(this);

        listViewJourneys.setAdapter(journeyAdapter);
        journeyAdapter.notifyDataSetChanged();
        routeListView.setAdapter(routeAdapter);
        routeAdapter.notifyDataSetChanged();

        registerForContextMenu(listViewJourneys);
        registerForContextMenu(routeListView);
        refreshJourneys();

        addJourneyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddJourneyDialogActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        listViewJourneys.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Journey journey = (Journey) parent.getItemAtPosition(position);
                journeyTitleRouteSection.setText(journey.getName());
                if (lastLoadedJourney != position) {
                    lastLoadedJourney = position;
                    Services.JOURNEY_SERVICE.getRouteByJourney(new routeLoader(), getContext(), journey.getId());
                }
                switcher.setOutAnimation(slideRight);
                switcher.setInAnimation(slideLeftBack);
                switcher.showNext();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switcher.setInAnimation(slideRightBack);
                switcher.setOutAnimation(slideLeft);
                switcher.showNext();
            }
        });
        Services.JOURNEY_SERVICE.getJourneys(new journeyLoader(), getContext());
        return view;
    }


    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId() == R.id.myfavorite_journeylistview){
            MenuInflater inflater = getActivity().getMenuInflater();
            menu.setHeaderTitle(R.string.header_title_journeys_listview);
            inflater.inflate(R.menu.menu_myfavorite_journeys_context, menu);
        }
        if(v.getId() == R.id.journey_fragment_alternate_route_list){
            MenuInflater inflater = getActivity().getMenuInflater();
            menu.setHeaderTitle(R.string.header_title_journeys_listview);
            inflater.inflate(R.menu.menu_myfavorite_routes_context, menu);
        }
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        position_item = info.position;
    }


    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            //Menu journey
            case R.id.play_journey:
                //TODO: Faire jouer la sélection complète du journey;
                return true;
            case R.id.pause_journey:
                //TODO: Mettre sur pause la sélection complète du journey;
                return true;
            case R.id.delete_journey:
                long id_journey = journeyAdapter.getItem(position_item).getId();
                Services.JOURNEY_SERVICE.deleteJourney(this, getContext(), id_journey);
                refreshJourneys();
                return true;
            case R.id.cancel_journey:
                return super.onContextItemSelected(item);

            //Menu route
            case R.id.play_route:
                //TODO: Faire jouer la route
            case R.id.pause_route:
                //TODO: Mettre sur pause la route
            case R.id.delete_route:
                //TODO supprimer une route d'une journey
            case R.id.cancel_route:
                return super.onContextItemSelected(item);
            default:
                return super.onContextItemSelected(item);

        }
    }

    public void showLoadingDialog() {
        if (loadingDialog == null)
            loadingDialog = LoadingDialog.newInstance();
        if (!loadingDialog.isShowing())
            loadingDialog.show(getFragmentManager(), LoadingDialog.TAG);
    }

    public void dismissLoadingDialog() {
        if (loadingDialog != null && loadingDialog.isShowing())
            loadingDialog.dismiss();
    }

    public void refreshJourneys() {
        Services.JOURNEY_SERVICE.getJourneys(new journeyLoader(), getContext());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
        lastLoadedJourney = -1;
    }

    @Override
    public void onTaskCompleted(WebServiceResult result) {
        dismissLoadingDialog();
        if (result.isSuccess()) {
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onProgress(Integer... value) {

    }

    @Override
    public void onPreExecute(Object obj) {
        showLoadingDialog();
    }

    @Override
    public void onTaskCancelled(WebServiceResult result) {
        dismissLoadingDialog();
    }

    @Override
    public void onClose() {
        refreshJourneys();
    }


    private class routeLoader implements IDataServiceCaller {

        @Override
        public void onTaskCompleted(WebServiceResult result) {
            routeAdapter.clear();
            if (result != null && result.isSuccess()) {
                Journey journey = result.getResult(Journey.class);
                routeAdapter.addAll(journey.getRoutes());
            }
            routeAdapter.notifyDataSetChanged();
        }

        @Override
        public void onProgress(Integer... value) {

        }

        @Override
        public void onPreExecute(Object obj) {

        }

        @Override
        public void onTaskCancelled(WebServiceResult result) {

        }
    }

    private class journeyLoader implements IDataServiceCaller {

        @Override
        public void onTaskCompleted(WebServiceResult result) {
            if (result != null && result.isSuccess()) {
                List<Journey> journeys = (List) result.getResult();
                journeyAdapter.clear();
                journeyAdapter.addAll(journeys);
                journeyAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onProgress(Integer... value) {

        }

        @Override
        public void onPreExecute(Object obj) {

        }

        @Override
        public void onTaskCancelled(WebServiceResult result) {

        }
    }
}