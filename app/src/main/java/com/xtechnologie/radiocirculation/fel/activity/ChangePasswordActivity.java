package com.xtechnologie.radiocirculation.fel.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.fel.dialog.NoAccountCreationDialog;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText answer, password, confirmPassword, email;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        answer = (EditText) findViewById(R.id.change_secret_answer);
        password = (EditText) findViewById(R.id.change_user_password);
        confirmPassword = (EditText) findViewById(R.id.change_confirm_password);
        email = (EditText) findViewById(R.id.change_user_email);
        spinner = (Spinner) findViewById(R.id.change_secret_question_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.questions_spinner, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);


        ((Button) findViewById(R.id.change_pwd_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: implements the password change
                Intent intent = new Intent(ChangePasswordActivity.this, SplashScreen.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
