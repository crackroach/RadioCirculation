package com.xtechnologie.radiocirculation.fel.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.fel.fragment.MyFavoriteFragment;

import java.util.ArrayList;
import java.util.List;

public class AddJourneyDialogActivity extends AppCompatActivity implements IDataServiceCaller {
    private EditText journeyName;
    private Button addJourney, cancelButton;
    private static List<AddJourneyDialogOnClose> observers;
    private List<Route> routes;
    public static final String ROUTES = "ROUTES";

    public interface AddJourneyDialogOnClose{
        public void onClose();
    }

    public static void register(AddJourneyDialogOnClose o){
        if(observers == null){
            observers = new ArrayList<>();
        }
        observers.add(o);
    }

    private void close(){
        if(observers != null)
            for(AddJourneyDialogOnClose o : observers){
                if(o != null)
                    o.onClose();
            }
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent().getExtras() != null)
        {
            routes = getIntent().getExtras().getParcelableArrayList(ROUTES);
        }
        setContentView(R.layout.activity_add_journey_dialog);

        journeyName = (EditText) findViewById(R.id.journeyNameText);
        addJourney = (Button) findViewById(R.id.button_ok_addJourney);
        cancelButton = (Button) findViewById(R.id.button_cancel_addJourney);

        addJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = journeyName.getText().toString();
                if(routes == null) routes = new ArrayList<Route>();
                Services.JOURNEY_SERVICE.createJourney(AddJourneyDialogActivity.this, getApplicationContext(), name, routes.toArray(new Route[routes.size()]));
                finish();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onTaskCompleted(WebServiceResult result) {
        close();
    }

    @Override
    public void onProgress(Integer... value) {

    }

    @Override
    public void onPreExecute(Object obj) {

    }

    @Override
    public void onTaskCancelled(WebServiceResult result) {

    }
}
