package com.xtechnologie.radiocirculation.fel.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.fel.activity.AddJourneyDialogActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexis on 2017-04-29.
 */

public class SelectJourneyDialog extends DialogFragment
{
    public static final String ROUTES_KEY = "routes";

    private List<Integer> routesID;
    private LoadingDialog loading;
    private ArrayAdapter<String> adapter;
    private ListView journyeList;
    private Button newJourneyButton;

    public static SelectJourneyDialog newInstance(List<Integer> routes)
    {
        SelectJourneyDialog instance = new SelectJourneyDialog();
        Bundle args = new Bundle();
        args.putIntegerArrayList(ROUTES_KEY, (ArrayList) routes);

        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
        {
            routesID = getArguments().getIntegerArrayList(ROUTES_KEY);
        }
        else
        {
            Intent intent = new Intent(getContext(), AddJourneyDialogActivity.class);
            startActivityForResult(intent, 1);
            dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_journey_selection, container, false);
        newJourneyButton = (Button) view.findViewById(R.id.journey_selection_fragment_new_journey);
        journyeList = (ListView) view.findViewById(R.id.journey_selection_fragment_journeylist);
        adapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1, new ArrayList<String>());

        journyeList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
//                long journeyID = adapter.getItem(position).get;
                Toast.makeText(getContext(), "clicky", Toast.LENGTH_SHORT)
                     .show();
            }
        });

        Services.JOURNEY_SERVICE.getJourneys(new IDataServiceCaller()
        {
            @Override
            public void onTaskCompleted(WebServiceResult result)
            {
                loading.dismiss();
                loading = null;
                if(result.isSuccess())
                {
                    adapter.clear();
                    List<String> str = new ArrayList<String>();

                    for(Journey j : new ArrayList<Journey>(result.getResult(ArrayList.class)))
                        str.add(j.getName());

                    adapter.addAll(str);
                }
            }

            @Override
            public void onProgress(Integer... value)
            {

            }

            @Override
            public void onPreExecute(Object obj)
            {
                loading = LoadingDialog.newInstance();
                loading.show(getFragmentManager(), "Loading");
            }

            @Override
            public void onTaskCancelled(WebServiceResult result)
            {
                loading.dismiss();
                loading = null;
            }
        }, getContext());

        return view;
    }
}
