package com.xtechnologie.radiocirculation.fel.activity;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;

import com.facebook.stetho.Stetho;
import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.androidservice.AudioServiceController;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.fel.fragment.MapWebViewFragment;
import com.xtechnologie.radiocirculation.fel.fragment.MyFavoriteFragment;
import com.xtechnologie.radiocirculation.fel.fragment.SectorListFragmentAlternate;
import com.xtechnologie.radiocirculation.utils.view.SwipelessViewPager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MediaController.MediaPlayerControl
{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private SwipelessViewPager mViewPager;
    private List<Fragment> pages;
    // augmenter ce nombre pour ajouter des pages
    private final int NB_PAGES = 3;
    private AudioServiceController audioController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Création de la liste qui va accueillir toutes les Fragments pour les afficher sur le ViewPager
         */
        pages = new ArrayList<>(NB_PAGES);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (SwipelessViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        /**
         * Ajout des pages de notre application sur le ViewPager
         *
         * L'ordre est important
         */

        pages.add(SectorListFragmentAlternate.newInstance());
        pages.add(MapWebViewFragment.newInstance());
        pages.add(MyFavoriteFragment.newInstance());

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setupAudioController();
    }

    private void setupAudioController()
    {
        audioController = Services.AUDIO_MANAGER.getAudioServiceControllerInstance(this);
        audioController.setPrevNextListeners(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playNext();
            }
        }, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playPrevious();
            }
        });
        audioController.setAnchorView(findViewById(R.id.main_content));
        audioController.setEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // TODO: Ajouter des options si on en veut ici
        int id = item.getItemId();

        switch (id)
        {
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Services.AUDIO_MANAGER.showNotificationPlayer();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Services.AUDIO_MANAGER.dismissNotificationPlayer();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            return pages.get(position);
        }

        @Override
        public int getCount() {
            // Afficheun total de NB_PAGES pages.
            return NB_PAGES;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.tab_layout_title_sectors);
                case 1:
                    return getResources().getString(R.string.tab_layout_title_map);
                case 2:
                    return getResources().getString(R.string.tab_layout_title_favorite);
            }
            return null;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setupAudioController();
        if (isPlaying())
            Services.AUDIO_MANAGER.show(0);
        Services.AUDIO_MANAGER.dismissNotificationPlayer();
    }

    public void playNext()
    {
        Services.AUDIO_MANAGER.playNext();
        audioController.show(0);
    }

    public void playPrevious()
    {
        Services.AUDIO_MANAGER.playPrevious();
        audioController.show(0);
    }

    @Override
    public void start()
    {
        Services.AUDIO_MANAGER.start();
    }

    @Override
    public void pause()
    {
        Services.AUDIO_MANAGER.pause();
    }

    @Override
    public int getDuration()
    {
        return Services.AUDIO_MANAGER.getDuration();
    }

    @Override
    public int getCurrentPosition()
    {
        return Services.AUDIO_MANAGER.getCurrentPosition();
    }

    @Override
    public void seekTo(int pos)
    {
        Services.AUDIO_MANAGER.seekTo(pos);
    }

    @Override
    public boolean isPlaying()
    {
        return Services.AUDIO_MANAGER.isPlaying();
    }

    @Override
    public int getBufferPercentage()
    {
        return Services.AUDIO_MANAGER.getBufferPercentage();
    }

    @Override
    public boolean canPause()
    {
        return Services.AUDIO_MANAGER.canPause();
    }

    @Override
    public boolean canSeekBackward()
    {
        return Services.AUDIO_MANAGER.canSeekBackward();
    }

    @Override
    public boolean canSeekForward()
    {
        return Services.AUDIO_MANAGER.canSeekForward();
    }

    @Override
    public int getAudioSessionId()
    {
        return Services.AUDIO_MANAGER.getAudioSessionId();
    }
}
