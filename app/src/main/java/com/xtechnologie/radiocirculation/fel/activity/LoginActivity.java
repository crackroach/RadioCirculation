package com.xtechnologie.radiocirculation.fel.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.web.User;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.fel.dialog.NoAccountCreationDialog;

public class LoginActivity extends AppCompatActivity implements IDataServiceCaller
{
    public static final String IS_FROM_SETTINGS = "settings";

    private EditText email, password;
    private boolean isFromAnotherActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getIntent() != null)
        {
            isFromAnotherActivity = getIntent().getBooleanExtra(IS_FROM_SETTINGS, false);
        }
        setContentView(R.layout.activity_login);

        password = (EditText) findViewById(R.id.login_user_password);
        email = (EditText) findViewById(R.id.login_user_email);

//        if(isFromAnotherActivity)
//            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        // L'action pour créer un nouveau compte
        ((Button) findViewById(R.id.login_open_account)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(LoginActivity.this, CreateAccountActivity.class);
                startActivity(intent);

            }
        });

        // L'action pour changer le mot de passe
        ((Button) findViewById(R.id.login_forgot_password)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                startActivity(intent);

            }
        });

        // L'action pour ceux qui ne veulent pas se créer de compte
        ((Button) findViewById(R.id.login_open_without_account)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoAccountCreationDialog.newInstance(LoginActivity.this).show(getSupportFragmentManager(), null);
            }
        });

        // L'action pour se connecter
        ((Button) findViewById(R.id.login_login)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity activity = LoginActivity.this;
                String email = activity.email.getText().toString();
                String password = activity.password.getText().toString();

                // Si le user peut se connecter
                if(!password.isEmpty()
                        || !email.isEmpty()
                        || Patterns.EMAIL_ADDRESS.matcher(email).matches())
                {
                    Services.USER_SERVICE.login(email, password, LoginActivity.this);
                }
                else
                {
                    Toast.makeText(activity, R.string.user_credentials_invalid, Toast.LENGTH_SHORT)
                         .show();
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(WebServiceResult result)
    {
        if(result != null)
        {
            if (result != null && result.isSuccess())
            {
                if (result.getResult() instanceof User)
                {
                    if(!isFromAnotherActivity)
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                    return;
                }
            }
            else
            {
                if (result.getErrno() == 2)
                {
                    Toast.makeText(this, R.string.user_credentials_invalid, Toast.LENGTH_SHORT)
                         .show();
                }
                return;
            }
        }
        Toast.makeText(this, R.string.unknown_error, Toast.LENGTH_SHORT)
             .show();
    }

    @Override
    public void onProgress(Integer... value)
    {

    }

    @Override
    public void onPreExecute(Object obj)
    {
        restore();
    }

    @Override
    public void onTaskCancelled(WebServiceResult result)
    {
        restore();
    }

    /**
     * Permet de remettre la fenêtre à neuf
     */
    private void restore()
    {
        password.getText().clear();
        email.getText().clear();
        email.requestFocus();
    }
}
