package com.xtechnologie.radiocirculation.fel.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;

import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-05-02.
 */

public class SimpleJourneyAdapter extends ArrayAdapter<Journey>
{
    public SimpleJourneyAdapter(Context ctx, List<Journey> journeys)
    {
        super(ctx, 0, journeys);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_simple_journey_item,parent, false);
        }

        SimpleJourneyViewHolder viewHolder = (SimpleJourneyViewHolder) convertView.getTag();

        if(viewHolder == null)
        {
            viewHolder = new SimpleJourneyViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.simple_journey_item_title);
            convertView.setTag(viewHolder);
        }

        Journey journey = getItem(position);
        if(journey != null)
            viewHolder.title.setText(journey.getName());
        else
            viewHolder.title.setText("Erreur");

        return convertView;
    }

    private class SimpleJourneyViewHolder
    {
        public TextView title;
    }
}
