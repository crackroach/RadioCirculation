package com.xtechnologie.radiocirculation.fel.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;

public class CreateAccountActivity extends AppCompatActivity implements IDataServiceCaller
{

    private EditText email, password, confirmPassword, answer, name;
    private Spinner questionChoiceSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        email = (EditText) findViewById(R.id.register_user_email);
        password = (EditText) findViewById(R.id.register_user_password);
        confirmPassword = (EditText) findViewById(R.id.register_confirm_password);
        answer = (EditText) findViewById(R.id.register_secret_answer);
        questionChoiceSpinner = (Spinner) findViewById(R.id.register_question_spinner);
        name = (EditText) findViewById(R.id.register_user_name);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.questions_spinner, android.R.layout.simple_spinner_item);
        questionChoiceSpinner.setAdapter(adapter);


        ((Button) findViewById(R.id.register_button)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String eml = email.getText().toString();
                String pwd = password.getText().toString();
                String cpwd = confirmPassword.getText().toString();
                int sq = questionChoiceSpinner.getSelectedItemPosition();
                String an = answer.getText().toString();
                String na = name.getText().toString();

                if(Services.APP.isValidEmail(eml))
                {
                    if(!pwd.isEmpty() && !cpwd.isEmpty() && pwd.compareTo(cpwd) == 0)
                    {
                        Services.USER_SERVICE.signUp(CreateAccountActivity.this, eml, pwd, na, sq, an);
                        // TODO: ajouter une animation pour montrer que l'aplication travaille
                    }
                    else
                        Toast.makeText(CreateAccountActivity.this, R.string.register_password_do_not_match, Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(CreateAccountActivity.this, R.string.register_need_valid_email, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void restore()
    {
        email.setText("");
        password.setText("");
        confirmPassword.setText("");
        answer.setText("");
        email.requestFocus();
    }

    @Override
    public void onTaskCompleted(WebServiceResult result)
    {
        if (result != null)
        {
            if(result.isSuccess())
            {
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return;
            }
            else
            {
                Toast.makeText(this, R.string.create_account_user_already_exists, Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, R.string.app_no_connection_found, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProgress(Integer... value) {}

    @Override
    public void onPreExecute(Object obj)
    {

    }

    @Override
    public void onTaskCancelled(WebServiceResult result)
    {

    }
}
