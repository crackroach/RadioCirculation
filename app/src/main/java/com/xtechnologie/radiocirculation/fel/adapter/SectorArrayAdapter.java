package com.xtechnologie.radiocirculation.fel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.fel.viewholder.SectorViewHolder;

import java.util.List;

/**
 * Created by Maxime on 2017-04-13.
 */

public class SectorArrayAdapter extends ArrayAdapter<Sector> {
    public SectorArrayAdapter(Context context, List<Sector> sectors)
    {
        super(context, 0, sectors);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        //Si la vue n'existe pas on va la créer
        if(convertView == null)
        {
            // on va chercher quel view va être le réceptacle de nos informations
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_sector,parent, false);
        }

        // Un viewHolder est défini dans un autre fichier afin d'accomoder le transfert d'information
        SectorViewHolder viewHolder = (SectorViewHolder) convertView.getTag();

        // si le viewHolder est null on va le créer
        if(viewHolder == null)
        {
            // Nouveau viewHolder
            viewHolder = new SectorViewHolder();

            // on lie chaque champs de notre viewholder à notre vue actuel, le convertView
            viewHolder.name = (TextView) convertView.findViewById(R.id.sector_name_txtView);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Exam> exams
        Sector sector = getItem(position);

        // On place chaques informations dans son TextView respectif
        viewHolder.name.setText(sector.getName());

        // on retourne notre vue pour que la liste l'affiche
        return convertView;
    }
}
