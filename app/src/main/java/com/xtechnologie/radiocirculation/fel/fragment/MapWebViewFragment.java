package com.xtechnologie.radiocirculation.fel.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.xtechnologie.radiocirculation.R;

public class MapWebViewFragment extends Fragment {


    public MapWebViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapWebViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapWebViewFragment newInstance() {
        MapWebViewFragment fragment = new MapWebViewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_web_view, container,false);
        WebView webview = (WebView) view.findViewById(R.id.fragment_mapwebview_webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl("http://www.xltechnologie.com/radiocirculation/map/map.html");
        return view;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
