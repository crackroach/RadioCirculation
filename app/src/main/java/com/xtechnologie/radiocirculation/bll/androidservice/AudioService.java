package com.xtechnologie.radiocirculation.bll.androidservice;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.MediaController;

import com.xtechnologie.radiocirculation.App;
import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.fel.activity.MainActivity;
import com.xtechnologie.radiocirculation.utils.ControllableArrayList;
import com.xtechnologie.radiocirculation.utils.ControllableList;
import com.xtechnologie.radiocirculation.utils.Preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexis on 2017-05-05.
 */

public class AudioService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener
{

    public interface OnPlayerPreparedListener
    {
        void onMediaPlayerPrepared();
    }

    public interface OnTrackCompletedListener
    {
        void onTrackCompleted(boolean isLastTrack);
    }

    private MediaPlayer player;
    private ControllableList<Route> playList;
    private int currentPosition;
    // Ce champs permet de garder le service audio actif même lorsque l'app n'est aps en avant plan. Quand nous perdons le binding au service, le mediaplayer s'éteint automatiquement.
    private final IBinder audioBinder = new AudioBinder();
    private boolean paused = false;
    private AudioPlayerNotification notification;

    private List<OnTrackCompletedListener> completionListeners;
    private List<OnPlayerPreparedListener> preparedListeners;

    @Override
    public void onCreate()
    {
        super.onCreate();
        currentPosition = 0;
        player = new MediaPlayer();
        initMusicPlayer();
        playList = new ControllableArrayList<>();
    }

    /**
     * Initialisation des paramètres audio
     */
    public void initMusicPlayer()
    {
        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    public List<Route> getPlayList()
    {
        return playList;
    }

    public void setPlayList(@NonNull List<Route> playList)
    {
        this.playList = new ControllableArrayList<>(playList);
    }

    public void registerToCompletion(OnTrackCompletedListener l)
    {
        if(completionListeners == null)
            completionListeners = new ArrayList<>();
        completionListeners.add(l);
    }

    public void unregisterToCompletion(MediaPlayer.OnCompletionListener l)
    {
        if(completionListeners != null)
            completionListeners.remove(l);
    }

    public void registerToPreparation(OnPlayerPreparedListener l)
    {
        if(preparedListeners == null)
            preparedListeners = new ArrayList<>();
        preparedListeners.add(l);
    }

    public void unregisterToPrepartion(MediaPlayer.OnPreparedListener l)
    {
        if(preparedListeners != null)
            preparedListeners.remove(l);
    }

    public void dismissNotificationPlayer()
    {
        if(notification != null)
            notification.cancel();
    }

    public void play(@NonNull List<Route> playList)
    {
        setPlayList(playList);
        play();
    }

    /**
     * On démarre le service audio pour jouer le son
     */
    public void play()
    {
        // Réinitialisation du lecteur pour enlever les erreurs et les lectures présentes
        player.reset();
        // Si notre playlist n'est pas vide
        if(!playList.isEmpty())
        {
            // Méthode de la liste de lecture qui est une extension de ArrayList
            Route route = playList.current();
            if(route != null)
            {
                try
                {
                    // Le path de l'audio à jouer
                    String audioPath = Preferences.URLS.AUDIO_URL + '/' + route.getAudioPath() + ".mp3";
                    // On met le URL comme source pour l'audio
                    player.setDataSource(audioPath);
                    // On prépare le lecteur asyncrone. Les ressources sont en ligne donc c'Est logique
                    // Une fois la préparation du lecteur faites on va aller dans la méthode onPrepared()
                    player.prepareAsync();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                return;
            }
        }
        // Si la playlist est vide, on relache la ressource Audio pour pouvoir libérer un peu d'espace
        release();
    }

    public void setRoute(int routeIndex)
    {
        currentPosition = routeIndex;
    }

    public MediaPlayer getPlayer()
    {
        return player;
    }

    /**
     * Permet de retourner le IBinder qui garde le lecteur actif
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return audioBinder;
    }

    /**
     * Lorsque le IBinder se désinscrit, le service audio éteint le mediaplayer
     * @param intent
     * @return
     */
    @Override
    public boolean onUnbind(Intent intent)
    {
        player.stop();
        player.release();
        if(notification != null)
            notification.cancel();
        stopForeground(true);
        return false;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        player.stop();
        player.release();
    }

    /**
     * Lorsque la lecture de la piste se termine, on vérifie que nous n'avons plus de piste à jouer
     *
     * Si on en a encore, on joue la prochaine
     * sinon on arrete le mediaplayer
     * @param mp
     */
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        boolean lastTrack = false;
        if(playList.hasNext())
        {
            playList.next();
            currentPosition++;
            play();
            lastTrack = true;
            updateNotificationPlayer();
        }
        else
        {
            if(notification != null)
                notification.cancel();
            release();
        }

        for(OnTrackCompletedListener l : completionListeners)
            l.onTrackCompleted(lastTrack);

    }

    private void updateNotificationPlayer()
    {
        if(notification != null)
        {
            Route rte = playList.current();
            if(rte != null)
            {
                notification.setNotificationInfo(rte.getSector()
                                                    .getName(), rte.getName());
                notification.show();
            }
            else
                notification.cancel();
        }
    }

    /**
     * Gestion des erreurs de lecture...
     */
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        player.reset();
        ((ControllableArrayList) playList).flush();

        return false;
    }

    /**
     * Lorsque le mediaplayer est pret, on démarre la lecture
     * @param mp
     */
    @Override
    public void onPrepared(MediaPlayer mp)
    {
        start();
        for(OnPlayerPreparedListener l : preparedListeners)
            l.onMediaPlayerPrepared();
    }


    public class AudioBinder extends Binder
    {
        public AudioService getService()
        {
            return AudioService.this;
        }
    }

    private void release()
    {
        player.stop();
    }

    public int getPosition()
    {
        return player.getCurrentPosition();
    }

    public int getDuration()
    {
        return player.getDuration();
    }

    public boolean isPlaying()
    {
        return player.isPlaying();
    }

    public boolean isPaused()
    {
        return paused;
    }

    public void pausePlayer()
    {
        player.pause();
        paused = true;
    }

    public void seek(int pos)
    {
        player.seekTo(pos);
    }

    public void start()
    {
        player.start();
        paused = false;
    }

    public boolean canPause()
    {
        return !player.isPlaying();
    }

    public void playNext()
    {
        if(playList.hasNext())
        {
            playList.next();
            play();
        }

    }

    public void playPrevious()
    {
        if(playList.hasPrevious())
        {
            playList.previous();
            play();
        }
    }

    public void showNotificationPlayer()
    {
        if(Services.APP.useNotificationPlayer(this))
        {
            notification = new AudioPlayerNotification(this);
            updateNotificationPlayer();
        }
/*
        Intent intent = new Intent(App.getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(App.getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(App.getContext());

        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_radiocircul_color_white)
                .setTicker("NOTIF")
                .setOngoing(true)
                .setContentTitle(playList.current().getName())
                .setContentText("NOTIF");
        Notification notification = builder.build();
        startForeground(0, notification);
*/
    }
}
