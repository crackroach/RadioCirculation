package com.xtechnologie.radiocirculation.bll.model.road;

import android.os.Parcel;
import android.os.Parcelable;

import com.xtechnologie.radiocirculation.bll.model.web.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class Route extends Zone implements Parcelable
{
    public static Parcelable.Creator<Route> CREATOR = new Parcelable.Creator<Route>()
    {
        @Override
        public Route createFromParcel(Parcel source)
        {
            return new Route(source);
        }

        @Override
        public Route[] newArray(int size)
        {
            return new Route[0];
        }
    };

    protected RouteType routeType;
    protected Sector sector;
    protected String audio;

    public Route(long id, String name)
    {
        super(id, name);
    }

    public Route(long id, String name, RouteType routeType, Sector sector, String audio)
    {
        super(id, name);
        this.routeType = routeType;
        this.sector = sector;
        this.audio = audio;
    }

    public Route(long id, String name, String audio)
    {
        super(id, name);
        this.audio = audio;
    }

    public Route(long id, String name, RouteType routeType, String audio)
    {
        super(id, name);
        this.routeType = routeType;
        this.audio = audio;
    }

    public Route(Parcel src)
    {
        super(src);
        this.routeType = src.readParcelable(RouteType.class.getClassLoader());
        this.sector = src.readParcelable(Sector.class.getClassLoader());
        this.audio = src.readString();
    }

    public RouteType getRouteType()
    {
        return routeType;
    }

    public void setRouteType(RouteType routeType)
    {
        this.routeType = routeType;
    }

    public Sector getSector()
    {
        return sector;
    }

    public void setSector(Sector sector)
    {
        this.sector = sector;
    }

    public String getAudio()
    {
        return audio;
    }

    public void setAudio(String audio)
    {
        this.audio = audio;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Zone zone = (Zone) o;

        return id == zone.id;

    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "Route{" +
                "routeType=" + routeType +
                ", sector=" + sector +
                ", audio='" + audio + '\'' +
                "} " + super.toString();
    }

    @Override
    public int describeContents()
    {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(routeType, flags);
        dest.writeParcelable(sector, flags);
        dest.writeString(audio);
    }
}
