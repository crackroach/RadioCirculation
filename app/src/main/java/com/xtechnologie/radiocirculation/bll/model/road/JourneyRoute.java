package com.xtechnologie.radiocirculation.bll.model.road;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class JourneyRoute extends Route implements Parcelable
{
    public static Parcelable.Creator<JourneyRoute> CREATOR = new Parcelable.Creator<JourneyRoute>()
    {
        @Override
        public JourneyRoute createFromParcel(Parcel source)
        {
            return new JourneyRoute(source);
        }

        @Override
        public JourneyRoute[] newArray(int size)
        {
            return new JourneyRoute[0];
        }
    };

    private int index;
    private Journey journey;

    public JourneyRoute(long id, String name, Journey journey)
    {
        super(id, name);
        this.journey = journey;
    }

    public JourneyRoute(long id, String name, int index, Journey journey)
    {
        super(id, name);
        this.index = index;
        this.journey = journey;
    }

    public JourneyRoute(long id, String name, RouteType routeType, String audio, Journey journey)
    {
        super(id, name, routeType, audio);
        this.journey = journey;
    }

    public JourneyRoute(long id, String name, RouteType routeType, String audio, int index, Journey journey)
    {
        super(id, name, routeType, audio);
        this.index = index;
        this.journey = journey;
    }

    public JourneyRoute(Route route)
    {
        super(route.id, route.name, route.routeType, route.audio);
    }

    public JourneyRoute(Route route, Journey journey)
    {
        super(route.id, route.name, route.routeType, route.audio);
        this.journey = journey;
    }

    public JourneyRoute(Parcel src)
    {
        super(src);
        this.index = src.readInt();
        this.journey = src.readParcelable(Journey.class.getClassLoader());
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public Journey getJourney()
    {
        return journey;
    }

    public void setJourney(Journey journey)
    {
        this.journey = journey;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Zone zone = (Zone) o;

        return id == zone.id;

    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "JourneyRoute{" +
                "index=" + index +
                ", journey=" + journey +
                "} " + super.toString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeInt(index);
        dest.writeParcelable(journey, flags);
    }
}
