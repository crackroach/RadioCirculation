package com.xtechnologie.radiocirculation.bll.service;

import android.provider.ContactsContract;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 */

public class Services
{
    public static PhoneService PHONE_SERVICE = PhoneService.getInstance();
    public static DataService DATA = DataService.getInstance();
    public static UserService USER_SERVICE = UserService.getInstance();
    public static AppService APP = AppService.getInstance();
    public static JourneyService JOURNEY_SERVICE = JourneyService.getInstance();
    public static RouteService ROUTE_SERVICE = RouteService.getInstance();
    public static AudioServiceManager AUDIO_MANAGER = AudioServiceManager.getInstance();
}
