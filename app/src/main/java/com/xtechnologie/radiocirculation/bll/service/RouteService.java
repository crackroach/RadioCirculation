package com.xtechnologie.radiocirculation.bll.service;

import android.util.Log;

import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.Repository;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;

import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-04-13.
 */

public class RouteService
{
    private static RouteService _instance = null;

    private RouteService(){}

    public static RouteService getInstance()
    {
        if (_instance == null)
            _instance = new RouteService();
        return _instance;
    }

    /**
     * To get all the sectors
     *
     * @param caller
     */
    public void fetchAllSectors(IDataServiceCaller caller)
    {
        class Task implements IDataServiceCaller
        {
            private IDataServiceCaller caller;

            public Task(IDataServiceCaller caller)
            {
                this.caller = caller;
                Repository.getInstance().fetchAllSectors(this);
            }

            @Override
            public void onTaskCompleted(WebServiceResult result)
            {
                caller.onTaskCompleted(result);
            }

            @Override
            public void onProgress(Integer... value)
            {
                onProgress();
            }

            @Override
            public void onPreExecute(Object obj)
            {
                caller.onPreExecute(obj);
            }

            @Override
            public void onTaskCancelled(WebServiceResult result)
            {
                caller.onTaskCancelled(result);
            }
        }

        new Task(caller);
    }

    /**
     * To get the routes of a sector
     *
     * @param caller
     * @param sectorID
     */
    public void getRoutesBySector(IDataServiceCaller caller, final long sectorID)
    {
        class Task implements IDataServiceCaller
        {
            private IDataServiceCaller caller;

            public Task(IDataServiceCaller caller)
            {
                this.caller = caller;
                Repository.getInstance().getAllRoutesFromSector(this, sectorID);
            }

            @Override
            public void onTaskCompleted(WebServiceResult result)
            {
                caller.onTaskCompleted(result);
            }

            @Override
            public void onProgress(Integer... value)
            {
                caller.onProgress();
            }

            @Override
            public void onPreExecute(Object obj)
            {
                caller.onPreExecute(obj);
            }

            @Override
            public void onTaskCancelled(WebServiceResult result)
            {
                caller.onTaskCancelled(result);
            }
        }

        new Task(caller);
    }
}
