package com.xtechnologie.radiocirculation.bll.service;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.net.ConnectivityManagerCompat;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 */

public class PhoneService
{
    private static PhoneService _instance;

    private PhoneService(){}

    public static PhoneService getInstance()
    {
        if(_instance == null)
            _instance = new PhoneService();
        return _instance;
    }

    public boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
