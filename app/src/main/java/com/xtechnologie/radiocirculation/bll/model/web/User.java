package com.xtechnologie.radiocirculation.bll.model.web;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mathieu on 2017-04-02.
 */

public class User {
    
    private long id;
    private String email;
    private String password;
    private String question;
    private String answer;
    private String token;
    private String name;
    private List<Journey> journeys;

    public User(long id, String email, String token) {
        this.id = id;
        this.email = email;
        this.token = token;
        journeys = new ArrayList<>();
        this.name = "Inconnu";
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", token='" + token + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    //Getters and setters
    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Journey> getJourneys() {
        return journeys;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setJourneys(List<Journey> journeys) {
        this.journeys = journeys;
    }

    public boolean addJourney(Journey journey)
    {
        return journeys.add(journey);
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
