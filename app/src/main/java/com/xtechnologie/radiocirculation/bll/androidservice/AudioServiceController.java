package com.xtechnologie.radiocirculation.bll.androidservice;

import android.content.Context;
import android.widget.MediaController;

import com.xtechnologie.radiocirculation.bll.service.Services;

/**
 * Created by Alexis on 2017-05-07.
 */

public class AudioServiceController extends MediaController
{
    public AudioServiceController(Context ctx)
    {
        super(ctx);
    }

    public void hide()
    {
        if (!Services.AUDIO_MANAGER.isPaused())
            super.hide();
    }

}
