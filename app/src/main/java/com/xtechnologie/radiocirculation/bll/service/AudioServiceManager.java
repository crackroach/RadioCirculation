package com.xtechnologie.radiocirculation.bll.service;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;

import com.google.firebase.messaging.RemoteMessage;
import com.xtechnologie.radiocirculation.App;
import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.androidservice.AudioService;
import com.xtechnologie.radiocirculation.bll.androidservice.AudioServiceController;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.fel.activity.MainActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AudioServiceManager extends MediaController implements MediaController.MediaPlayerControl, AudioService.OnPlayerPreparedListener, AudioService.OnTrackCompletedListener
{
    private static AudioServiceManager _instance;

    private AudioService audioService;
    private Intent intent;
    private boolean musicBound = false;
    private List<Route> playList;
    private AudioServiceController controller;

    private AudioServiceManager() 
    {
        super(App.getContext());
        if(intent == null)
        {
            playList = new ArrayList<>();
            App ctx = App.getInstance();
            intent = new Intent(App.getContext(), AudioService.class);
            ctx.bindService(intent, audioConnection, Context.BIND_AUTO_CREATE);
            ctx.startService(intent);
        }
    }

    private void setup()
    {
        audioService.registerToCompletion(this);
        audioService.registerToPreparation(this);
    }
    
    public static AudioServiceManager getInstance()
    {
        if(_instance == null)
        {
            _instance = new AudioServiceManager();
        }
        return _instance;
    }

    public List<Route> getPlayList()
    {
        return playList;
    }

    public void setPlayList(List<Route> playList)
    {
        this.playList = playList;
    }

    public boolean isMusicBound()
    {
        return musicBound;
    }

    public void setMusicBound(boolean musicBound)
    {
        this.musicBound = musicBound;
    }

    public AudioServiceController getAudioServiceControllerInstance(@NonNull MediaPlayerControl playerControl)
    {
        controller = new AudioServiceController((MainActivity) playerControl);

        controller.setMediaPlayer(playerControl);

        return controller;
    }

    /**
     * On passe une liste à lire par le audioservice
     * @param routeList
     */
    public void play(@NonNull List<Route> routeList)
    {
        audioService.play(routeList);
        if (controller != null && !controller.isShowing())
        {
            controller.show(0);
            controller.refreshDrawableState();
        }
    }

    /**
     * ON joue une ou plusieurs pistes
     * @param routes
     */
    public void play(Route... routes)
    {
        if(routes != null && routes.length > 0)
            play(Arrays.asList(routes));
        else
            play(new ArrayList<Route>());
    }

    public void showNotificationPlayer()
    {
        audioService.showNotificationPlayer();
    }

    public void dismissNotificationPlayer()
    {
        audioService.dismissNotificationPlayer();
    }

    /**
     * La connexion au service de media
     */
    private ServiceConnection audioConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            AudioService.AudioBinder binder = (AudioService.AudioBinder) service;
            audioService = binder.getService();
            audioService.setPlayList(playList);
            audioService.registerToCompletion(AudioServiceManager.this);
            audioService.registerToPreparation(AudioServiceManager.this);
            musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            musicBound = false;
        }
    };


//*
    @Override
    public void start()
    {
        audioService.start();
    }

    @Override
    public void pause()
    {
        audioService.pausePlayer();
    }

    @Override
    public int getDuration()
    {
        if(audioService != null && musicBound && (audioService.isPaused() || audioService.isPlaying()))
            return audioService.getDuration();
        return 0;
    }

    @Override
    public int getCurrentPosition()
    {
        return audioService.getPosition();
    }

    @Override
    public void seekTo(int pos)
    {
        audioService.seek(pos);
    }

    @Override
    public boolean isPlaying()
    {
        return audioService.isPlaying();
    }

    @Override
    public int getBufferPercentage()
    {
        return 0;
    }

    @Override
    public boolean canPause()
    {
        return audioService.canPause();
    }

    @Override
    public boolean canSeekBackward()
    {
        return false;
    }

    @Override
    public boolean canSeekForward()
    {
        return true;
    }

    @Override
    public int getAudioSessionId()
    {
        return 0;
    }

    public void playNext()
    {
        audioService.playNext();
    }

    public void playPrevious()
    {
        audioService.playPrevious();
    }

    public boolean isPaused()
    {
        return audioService.isPaused();
    }

    @Override
    public void onTrackCompleted(boolean isLastTrack)
    {
        if (isLastTrack)
            controller.show(0);
        else
            controller.hide();
    }

    @Override
    public void onMediaPlayerPrepared()
    {
        controller.show(0);
    }

    //*/
}