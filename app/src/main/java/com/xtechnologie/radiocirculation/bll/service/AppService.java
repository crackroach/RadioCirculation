package com.xtechnologie.radiocirculation.bll.service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.xtechnologie.radiocirculation.utils.Preferences;
import com.xtechnologie.radiocirculation.utils.PreferencesKey;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 */

public class AppService
{
    private static AppService _instance;
    public final boolean IS_DEBUG = false;

    private AppService() {}

    public static AppService getInstance()
    {
        if(_instance == null)
            _instance = new AppService();
        return _instance;
    }

    public boolean isFirstRun(Context ctx)
    {
        boolean isFirstRun = true;

        PreferencesKey key = PreferencesKey.IS_FIRST_RUN;
        Preferences pref = Preferences.newInstance(ctx);

        if(IS_DEBUG)
            pref.clearAll();

        isFirstRun = pref.getBoolean(key);
        if(isFirstRun)
            pref.setBoolean(key, false);
        return isFirstRun;
    }

    public boolean isValidEmail(String email)
    {
        if(TextUtils.isEmpty(email))
            return false;
        else
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean useNotificationPlayer(Context ctx)
    {
        return Preferences.newInstance(ctx).getBoolean(PreferencesKey.USE_NOTIFICATION_PLAYER);
    }

    public void setNotificationPlayerEnabled(Context ctx, boolean enabled)
    {
        Preferences.newInstance(ctx).setBoolean(PreferencesKey.USE_NOTIFICATION_PLAYER, enabled);
    }
}
