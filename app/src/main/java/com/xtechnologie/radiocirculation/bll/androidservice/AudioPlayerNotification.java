package com.xtechnologie.radiocirculation.bll.androidservice;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.xtechnologie.radiocirculation.R;
import com.xtechnologie.radiocirculation.bll.service.Services;

/**
 * Created by Alexis on 2017-05-14.
 */

public class AudioPlayerNotification extends Notification
{
    private final int NOTIFICATION_ID = 548853;

    private Context ctx;
    private NotificationManager mNotificationManager;
    private Notification notification;
    private RemoteViews contentView;

    @SuppressLint("NewApi")
    public AudioPlayerNotification(Context ctx){
        super();

        this.ctx = ctx;
        String ns = Context.NOTIFICATION_SERVICE;
        mNotificationManager = (NotificationManager) ctx.getSystemService(ns);
        CharSequence tickerText = "Shortcuts";
        long when = System.currentTimeMillis();
        Notification.Builder builder = new Notification.Builder(ctx);
        notification = builder.getNotification();
        notification.when = when;
        notification.tickerText = tickerText;
        notification.icon = R.drawable.ic_radiocircul_color_white;



        contentView = new RemoteViews(ctx.getPackageName(), R.layout.activity_notification_player);



       // contentView.setString(R.id.notification_route_name, "setText", "Unknown");
        //contentView.setString(R.id.notification_sector_title, "setText", "Unknown");
        //set the button listeners
        setListeners(contentView);

        notification.contentView = contentView;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        //CharSequence contentTitle = "From Shortcuts";

    }

    public void cancel()
    {
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    public void show()
    {
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    public void setRouteName(String name)
    {
        contentView.setTextViewText(R.id.notification_route_name, name);
    }

    public void setSectorName(String name)
    {
        contentView.setTextViewText(R.id.notification_sector_title, name);
    }

    public void setNotificationInfo(String sectorName, String routeName)
    {
        setSectorName(sectorName);
        setRouteName(routeName);
    }

    public void setListeners(RemoteViews view){
        //radio listener
        /*
        Intent radio = new Intent(ctx,HelperActivity.class);
        radio.putExtra("DO", "radio");
        PendingIntent pRadio = PendingIntent.getActivity(ctx, 0, radio, 0);
        view.setOnClickPendingIntent(R.id.radio, pRadio);*/

        //volume listener
        /*Intent volume=new Intent(ctx, HelperActivity.class);
        volume.putExtra("DO", "volume");
        PendingIntent pVolume = PendingIntent.getActivity(ctx, 1, volume, 0);
        view.setOnClickPendingIntent(R.id.volume, pVolume);*/

        //reboot listener
        /*Intent reboot=new Intent(ctx, HelperActivity.class);
        reboot.putExtra("DO", "reboot");
        PendingIntent pReboot = PendingIntent.getActivity(ctx, 5, reboot, 0);
        view.setOnClickPendingIntent(R.id.reboot, pReboot);*/

        Intent next = new Intent(ctx, HelperActivity.class);
        next.putExtra("DO", "next ");
        PendingIntent pNext = PendingIntent.getActivity(ctx, 3, next, 0);
        view.setOnClickPendingIntent(R.id.notification_next, pNext);

        Intent play = new Intent(ctx, HelperActivity.class);
        play.putExtra("DO", "play");
        PendingIntent pApp = PendingIntent.getActivity(ctx, 4, play, 0);
        view.setOnClickPendingIntent(R.id.notification_play_pause, pApp);

        Intent previous = new Intent(ctx, HelperActivity.class);
        previous.putExtra("DO", "previous");
        PendingIntent pPrevious = PendingIntent.getActivity(ctx, 4, previous, 0);
        view.setOnClickPendingIntent(R.id.notification_previous, pPrevious);
    }
}
