package com.xtechnologie.radiocirculation.bll.service;

import android.content.Context;
import android.util.Log;

import com.xtechnologie.radiocirculation.bll.model.web.User;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.Repository;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.utils.Preferences;
import com.xtechnologie.radiocirculation.utils.PreferencesKey;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 */

public class UserService
{
    private static UserService _instance;

    private User currentUser;

    private UserService(){}

    public static UserService getInstance()
    {
        if(_instance == null)
            _instance = new UserService();
        return _instance;
    }

    public User getCurrentUser()
    {
        return currentUser;
    }

    public void loginWithToken(IDataServiceCaller caller)
    {
        class Task implements IDataServiceCaller
        {
            private IDataServiceCaller caller;

            public Task(IDataServiceCaller caller)
            {
                this.caller = caller;
                Preferences pref = Preferences.newInstance((Context)caller);
                String token = pref.getString(PreferencesKey.TOKEN);
                Repository.getInstance().getUser(token, this);
            }

            @Override
            public void onTaskCompleted(WebServiceResult result)
            {
                if(result != null && result.isSuccess())
                {
                    currentUser = (User) result.getResult();
                    Log.i("USER", currentUser.toString());

                }
                caller.onTaskCompleted(result);
            }

            @Override
            public void onProgress(Integer... value)
            {
                caller.onProgress();
            }

            @Override
            public void onPreExecute(Object obj)
            {
                caller.onPreExecute(obj);
            }

            @Override
            public void onTaskCancelled(WebServiceResult result)
            {
                caller.onTaskCancelled(result);
            }
        }

        new Task(caller);
    }

    public void login(String email, String rawPassword, IDataServiceCaller caller)
    {
        class Task implements IDataServiceCaller
        {
            private IDataServiceCaller caller;

            public Task(String email, String password, IDataServiceCaller caller)
            {
                this.caller = caller;

                Repository.getInstance().getUser(email, password, this);
            }

            //
            @Override
            public void onTaskCompleted(WebServiceResult result)
            {
                if(result != null && result.isSuccess())
                {
                    Preferences pref = Preferences.newInstance((Context) caller);
                    currentUser = (User) result.getResult();
                    pref.setString(PreferencesKey.TOKEN, currentUser.getToken());
                    caller.onTaskCompleted(result);
                    setAsLocalUser((Context) caller, false);

                }
                else
                    caller.onTaskCancelled(null);
            }

            @Override
            public void onProgress(Integer... value)
            {

            }

            @Override
            public void onPreExecute(Object obj)
            {
                caller.onPreExecute(obj);
            }

            @Override
            public void onTaskCancelled(WebServiceResult result)
            {
                caller.onTaskCancelled(result);
            }
        }

        new Task(email, rawPassword, caller);
    }

    public void signUp(IDataServiceCaller caller, final String email, final String password, String name, int questionIndex, String answer)
    {
        class Task implements IDataServiceCaller
        {

            private IDataServiceCaller caller;

            public Task(IDataServiceCaller caller, String email, String password, String name, int questionIndex, String answer)
            {
                this.caller = caller;
                Repository.getInstance().signUp(this, email, password, name, questionIndex, answer);
            }

            @Override
            public void onTaskCompleted(WebServiceResult result)
            {

                Preferences pref = Preferences.newInstance((Context) caller);
                if(result != null)
                {
                    pref.setString(PreferencesKey.TOKEN, result.getMessage());
                    pref.setBoolean(PreferencesKey.IS_LOCAL_USER, false);
                }
                else
                    result = new WebServiceResult(false, 0, null, "No data retreived");
                caller.onTaskCompleted(result);
            }

            @Override
            public void onTaskCancelled(WebServiceResult result)
            {
                caller.onTaskCancelled(result);
            }

            @Override
            public void onProgress(Integer... value)
            {

            }

            @Override
            public void onPreExecute(Object obj)
            {
                caller.onPreExecute(obj);
            }
        }
        new Task(caller, email, password, name, questionIndex, answer);
    }

    public boolean isLocalUser(Context ctx)
    {
        Preferences pref = Preferences.newInstance(ctx);
        boolean isLocalUser = pref.getBoolean(PreferencesKey.IS_LOCAL_USER);

        return isLocalUser;
    }

    public void setAsLocalUser(Context ctx, boolean isLocalUser)
    {
        Preferences pref = Preferences.newInstance(ctx);
        pref.setBoolean(PreferencesKey.IS_LOCAL_USER, isLocalUser);
    }

    public boolean hasUserTokenStored(Context ctx)
    {
        Preferences pref = Preferences.newInstance(ctx);
        return pref.hasValue(PreferencesKey.TOKEN);
    }

    public void disconnect(Context ctx)
    {
        Preferences pref = Preferences.newInstance(ctx);
        pref.removeKey(PreferencesKey.TOKEN);
        pref.setBoolean(PreferencesKey.IS_LOCAL_USER, false);
        currentUser = null;
    }

}
