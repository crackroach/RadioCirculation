package com.xtechnologie.radiocirculation.bll.model.account;

import android.os.Parcel;
import android.os.Parcelable;

import com.xtechnologie.radiocirculation.bll.model.road.Journey;

import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class User implements Parcelable
{
    public static Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>()
    {
        @Override
        public User createFromParcel(Parcel source)
        {
            return new User(source);
        }

        @Override
        public User[] newArray(int size)
        {
            return new User[0];
        }
    };

    private long id;
    private String email;
    private String question;
    private String answer;
    private String token;
    private String name;
    private List<Journey> journeys;

    public User(long id, String email, String token)
    {
        this.id = id;
        this.email = email;
        this.token = token;
    }

    public User(Parcel src)
    {
        this.id = src.readLong();
        this.email = src.readString();
        this.question = src.readString();
        this.answer = src.readString();
        this.token = src.readString();
        this.name = src.readString();
        this.journeys = src.readArrayList(Journey.class.getClassLoader());
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getQuestion()
    {
        return question;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }

    public String getAnswer()
    {
        return answer;
    }

    public void setAnswer(String answer)
    {
        this.answer = answer;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Journey> getJourneys()
    {
        return journeys;
    }

    public void setJourneys(List<Journey> journeys)
    {
        this.journeys = journeys;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return id == user.id;

    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", token='" + token + '\'' +
                ", name='" + name + '\'' +
                ", journeys=" + journeys +
                '}';
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(email);
        dest.writeString(question);
        dest.writeString(answer);
        dest.writeString(token);
        dest.writeString(name);
        dest.writeList(journeys);
    }
}
