package com.xtechnologie.radiocirculation.bll.model.server;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mathieu on 2017-04-02.
 */

public class Route implements Parcelable{

    public static Parcelable.Creator<Route> CREATOR = new Parcelable.Creator<Route>()
    {
        @Override
        public Route createFromParcel(Parcel source)
        {
            return new Route(source);
        }

        @Override
        public Route[] newArray(int size)
        {
            return new Route[size];
        }
    };

    private long id;
    private String name;
    private Sector sector;
    private Audio audio;
    private Image image;
    private String audioPath;
    private String routeType;

    public Route(long id, String name, Sector sector, Audio audio, Image image) {
        this.id = id;
        this.name = name;
        this.sector = sector;
        this.audio = audio;
        this.image = image;
    }

    public Route(long id, String name, String audioPath, String routeType) {
        this.id = id;
        this.name = name;
        this.audioPath = audioPath;
        this.routeType = routeType;
    }

    public Route(Parcel src)
    {
        this.id = src.readLong();
        this.name = src.readString();
        this.audioPath = src.readString();
        this.sector = src.readParcelable(Sector.class.getClassLoader());
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", audioPath='" + getAudioPath() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj)
    {
        return (obj instanceof Route)
                && (this.id == ((Route) obj).id);
    }

    //getters and setters
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getAudioPath()
    {
        if(audioPath != null && !audioPath.isEmpty())
            return audioPath;
        else if (audio != null)
            return audio.getName();
        return "";
    }

    public void setAudioPath(String audioPath)
    {
        this.audioPath = audioPath;
    }

    public String getRouteType()
    {
        return routeType;
    }

    public void setRouteType(String routeType)
    {
        this.routeType = routeType;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(getAudioPath());
        dest.writeParcelable(sector, 0);
    }
}
