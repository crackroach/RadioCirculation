package com.xtechnologie.radiocirculation.bll.model.web;

import com.xtechnologie.radiocirculation.bll.model.server.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mathieu on 2017-04-02.
 */

public class Journey {
    private long id;
    private String name;
    private User user;
    private List<Route> routes;
    private int size;

    public Journey(){}

    public Journey(long id, String name){
        this.id = id;
        this.name = name;
        this.size = -1;
    }

    public Journey(long id, String name, User user) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.size = -1;
        routes = new ArrayList<>();
    }

    public Journey(String name)
    {
        this.name = name;
        this.id = 0;
        this.size = -1;
    }

    @Override
    public String toString() {
        return "Journey{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    //getters and setters
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public int getSize()
    {
        if(routes == null || routes.isEmpty())
            return size;
        else
            return routes.size();
    }

    public void setSize(int size)
    {
        if(size < 0) throw new IllegalArgumentException("Cannot be under 0");
        this.size = size;
    }

    public boolean addRoute(Route route)
    {
        if(routes == null)
            routes = new ArrayList<>();
        return routes.add(route);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Journey journey = (Journey) o;

        return id == journey.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
