package com.xtechnologie.radiocirculation.bll.service;

import android.content.Context;

import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.Repository;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.dal.sqlite.JourneyManager;
import com.xtechnologie.radiocirculation.dal.sqlite.SQLiteDAO;

import java.util.List;

/**
 * Created by Mathieu on 2017-04-09.
 */

public class JourneyService {
    private static JourneyService _instance;

    private JourneyService(){}

    public static JourneyService getInstance()
    {
        if(_instance == null)
            _instance = new JourneyService();
        return _instance;
    }

    public JourneyManager getJourneyManager(Context context){
        return SQLiteDAO.getInstance(context).getJourneyManager();
    }

    public void createJourney(final IDataServiceCaller caller, Context context, final String journeyName, Route... routes)
    {
        if(Services.USER_SERVICE.isLocalUser(context)){
            JourneyManager manager = getJourneyManager(context);
            Journey journey = new Journey(journeyName);
            manager.open();
            manager.insertJourney(journey);
            List<Journey> journeys = manager.getAllJourneys();
            manager.close();
            caller.onTaskCompleted(new WebServiceResult(true, journeys, "réussi"));
        }else{
            Repository.getInstance().createJourney(new IDataServiceCaller() {
                @Override
                public void onTaskCompleted(WebServiceResult result) {
                    caller.onTaskCompleted(result);
                }

                @Override
                public void onProgress(Integer... value) {
                    caller.onProgress(value);
                }

                @Override
                public void onPreExecute(Object obj) {
                    caller.onPreExecute(obj);
                }

                @Override
                public void onTaskCancelled(WebServiceResult result) {
                    caller.onTaskCancelled(result);
                }
            }, journeyName, routes);
        }
    }

    public void deleteJourney(final IDataServiceCaller caller, Context context, final long id){
        if(Services.USER_SERVICE.isLocalUser(context)){
            JourneyManager manager = getJourneyManager(context);
            manager.open();
            manager.deleteJourney(id);
            List<Journey> journeys = manager.getAllJourneys();
            manager.close();
            caller.onTaskCompleted(new WebServiceResult(true, journeys, "réussi"));
        }else{
            Repository.getInstance().removeJourney(caller, id);
        }
    }

    public void getJourneys(final IDataServiceCaller caller, Context context){
        if(Services.USER_SERVICE.isLocalUser(context)){
            JourneyManager manager = getJourneyManager(context);
            manager.open();
            List<Journey> journeys = manager.getAllJourneys();
            manager.close();
            caller.onTaskCompleted(new WebServiceResult(true, journeys, "réussi"));
        }else{
            Repository.getInstance().getJourneys(new IDataServiceCaller() {
                @Override
                public void onTaskCompleted(WebServiceResult result) {
                    caller.onTaskCompleted(result);
                }

                @Override
                public void onProgress(Integer... value) {
                    caller.onProgress(value);
                }

                @Override
                public void onPreExecute(Object obj) {
                    caller.onPreExecute(obj);
                }

                @Override
                public void onTaskCancelled(WebServiceResult result) {
                    caller.onTaskCancelled(result);
                }
            }, Services.USER_SERVICE.getCurrentUser().getToken());
        }
    }

    public void deleteRouteFromJourney(final IDataServiceCaller caller, Context context, long journeyID, long routeID){
        if(Services.USER_SERVICE.isLocalUser(context)){
            JourneyManager manager = getJourneyManager(context);
            manager.open();
            Journey journey = manager.getJourneyById(journeyID);
            for (Route route : journey.getRoutes()){
                if (route.getId() == routeID){
                    journey.getRoutes().remove(route);
                }
            }
            List<Journey> journeys = manager.getAllJourneys();
            manager.close();
            caller.onTaskCompleted(new WebServiceResult(true, journeys, "réussi"));
        } else {
            Repository.getInstance().removeRouteFromJourney(new IDataServiceCaller()
            {
                @Override
                public void onTaskCompleted(WebServiceResult result)
                {
                    caller.onTaskCompleted(result);
                }

                @Override
                public void onProgress(Integer... value)
                {
                    caller.onProgress(value);
                }

                @Override
                public void onPreExecute(Object obj)
                {
                    caller.onPreExecute(obj);
                }

                @Override
                public void onTaskCancelled(WebServiceResult result)
                {
                    caller.onTaskCancelled(result);
                }
            }, journeyID, routeID);
        }
    }

    public void addRouteToJourney(final IDataServiceCaller caller, Context context, long journeyID, Route... route)
    {
        if(Services.USER_SERVICE.isLocalUser(context))
        {
            //TODO faire la portion local de la chose
            throw new NullPointerException();
        }
        else
        {
            Repository.getInstance().addRouteToJourney(new IDataServiceCaller()
            {
                @Override
                public void onTaskCompleted(WebServiceResult result) {caller.onTaskCompleted(result);}

                @Override
                public void onProgress(Integer... value) { caller.onProgress(value); }

                @Override
                public void onPreExecute(Object obj) { caller.onPreExecute(obj);}

                @Override
                public void onTaskCancelled(WebServiceResult result) { caller.onTaskCompleted(result);}
            }, journeyID, route);
        }
    }

    public void getRouteByJourney(final IDataServiceCaller caller, Context context, long journeyID)
    {
        if(Services.USER_SERVICE.isLocalUser(context))
        {
            // TODO
        }
        else
        {
            Repository.getInstance().getAllRouteFromJourney(new IDataServiceCaller()
            {
                @Override
                public void onTaskCompleted(WebServiceResult result)
                {
                    caller.onTaskCompleted(result);
                }

                @Override
                public void onProgress(Integer... value)
                {
                    caller.onProgress(value);
                }

                @Override
                public void onPreExecute(Object obj)
                {
                    caller.onPreExecute(obj);
                }

                @Override
                public void onTaskCancelled(WebServiceResult result)
                {
                    caller.onTaskCancelled(result);
                }
            }, journeyID);
        }
    }

}
