package com.xtechnologie.radiocirculation.bll.model.road;

import android.os.Parcel;
import android.os.Parcelable;

import com.xtechnologie.radiocirculation.utils.Preferences;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class RouteType implements Parcelable
{
    public static Parcelable.Creator<RouteType> CREATOR = new Parcelable.Creator<RouteType>()
    {
        @Override
        public RouteType createFromParcel(Parcel source)
        {
            return new RouteType(source);
        }

        @Override
        public RouteType[] newArray(int size)
        {
            return new RouteType[0];
        }
    };

    private long id;
    private String name;
    private String filePath;

    public RouteType(long id, String name, String filePath)
    {
        this.id = id;
        this.name = name;
        this.filePath = Preferences.URLS.IMAGE_URL + "/" + filePath;
    }

    public RouteType(Parcel src)
    {
        this.id = src.readLong();
        this.name = src.readString();
        this.filePath = src.readString();
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RouteType routeType = (RouteType) o;

        return id == routeType.id;

    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "RouteType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(filePath);
    }
}
