package com.xtechnologie.radiocirculation.bll.model.server;

/**
 * Created by Mathieu on 2017-04-02.
 */

public class Image {
    private long id;
    private String name;
    private Route route;

    public Image(long id, String name, Route route) {
        this.id = id;
        this.name = name;
        this.route = route;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    //getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
