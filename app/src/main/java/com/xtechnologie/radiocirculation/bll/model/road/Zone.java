package com.xtechnologie.radiocirculation.bll.model.road;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class Zone implements Parcelable
{
    public static Parcelable.Creator<Zone> CREATOR = new Parcelable.Creator<Zone>()
    {
        @Override
        public Zone createFromParcel(Parcel source)
        {
            return new Zone(source);
        }

        @Override
        public Zone[] newArray(int size)
        {
            return new Zone[0];
        }
    };

    protected long id;
    protected String name;

    public Zone(long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public Zone(Parcel src)
    {
        this.id = src.readLong();
        this.name = src.readString();
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Zone zone = (Zone) o;

        return id == zone.id;

    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "Zone{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
    }
}
