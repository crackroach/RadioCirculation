package com.xtechnologie.radiocirculation.bll.model.server;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mathieu on 2017-04-02.
 */

public class Audio implements Parcelable{
    public static Parcelable.Creator<Audio> CREATOR = new Parcelable.Creator<Audio>()
    {
        @Override
        public Audio createFromParcel(Parcel source)
        {
            return new Audio(source);
        }

        @Override
        public Audio[] newArray(int size)
        {
            return new Audio[0];
        }
    };

    private long id;
    private String name;
    private Route route;

    public Audio(long id, String name, Route route) {
        this.id = id;
        this.name = name;
        this.route = route;
    }
    public Audio(Parcel src)
    {
        this.id = src.readLong();
        this.name = src.readString();
    }

    @Override
    public String toString() {
        return "Audio{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    //getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
    }
}
