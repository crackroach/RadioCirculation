package com.xtechnologie.radiocirculation.bll.model.server;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mathieu on 2017-04-02.
 */

public class Sector implements Parcelable{

    public static Parcelable.Creator<Sector> CREATOR = new Parcelable.Creator<Sector>()
    {
        @Override
        public Sector createFromParcel(Parcel source)
        {
            return new Sector(source);
        }

        @Override
        public Sector[] newArray(int size)
        {
            return new Sector[0];
        }
    };

    private long id;
    private String name;
    private List<Route> routes;

    public Sector(long id, String name) {
        this.id = id;
        this.name = name;
        routes = new ArrayList<>();
    }

    public Sector(Parcel src)
    {
        this.id = src.readLong();
        this.name = src.readString();
    }

    @Override
    public String toString() {
        return "Sector{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sector sector = (Sector) o;

        return id == sector.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    //getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public boolean addRoute(Route route)
    {
        route.setSector(this);
        return routes.add(route);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
    }
}
