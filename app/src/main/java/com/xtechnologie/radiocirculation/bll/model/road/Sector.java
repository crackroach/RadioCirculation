package com.xtechnologie.radiocirculation.bll.model.road;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class Sector extends Zone implements Parcelable
{
    public static Parcelable.Creator<Sector> CREATOR = new Parcelable.Creator<Sector>()
    {
        @Override
        public Sector createFromParcel(Parcel source)
        {
            return new Sector(source);
        }

        @Override
        public Sector[] newArray(int size)
        {
            return new Sector[0];
        }
    };

    private List<Route> routes;

    public Sector(long id, String name)
    {
        super(id, name);
    }

    public Sector(long id, String name, List<Route> routes)
    {
        super(id, name);
        this.routes = routes;
    }

    public Sector(Parcel src)
    {
        super(src);
        this.routes = src.readArrayList(Route.class.getClassLoader());
    }

    public List<Route> getRoutes()
    {
        return routes;
    }

    public void setRoutes(List<Route> routes)
    {
        this.routes = routes;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Zone zone = (Zone) o;

        return id == zone.id;

    }

    public boolean addRoute(Route rte)
    {
        boolean bool = this.routes.add(rte);
        rte.setSector(this);
        return bool;
    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "Sector{" +
                "routes=" + routes +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);
        dest.writeList(routes);
    }
}
