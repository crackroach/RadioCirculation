package com.xtechnologie.radiocirculation.bll.model.road;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-04-21.
 */

public class Journey implements Parcelable
{
    public static Parcelable.Creator<Journey> CREATOR = new Parcelable.Creator<Journey>()
    {
        @Override
        public Journey createFromParcel(Parcel source)
        {
            return new Journey(source);
        }

        @Override
        public Journey[] newArray(int size)
        {
            return new Journey[0];
        }
    };

    private long id;
    private String name;
    private List<JourneyRoute> routes;

    public Journey(long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public Journey(long id, String name, List<JourneyRoute> routes)
    {
        this.id = id;
        this.name = name;
        this.routes = routes;
    }

    public Journey(Parcel src)
    {
        this.id = src.readLong();
        this.name = src.readString();
        this.routes = src.readArrayList(JourneyRoute.class.getClassLoader());
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<JourneyRoute> getRoutes()
    {
        return routes;
    }

    public void setRoutes(List<JourneyRoute> routes)
    {
        this.routes = routes;
    }

    public boolean addRoute(Route rte)
    {
        boolean bool = false;

        JourneyRoute jr = new JourneyRoute(rte);
        if((bool = routes.add(jr)))
        {
            jr.setJourney(this);
            jr.setIndex(routes.indexOf(jr));
        }
        return bool;
    }

    public boolean addRoute(JourneyRoute route)
    {
        boolean bool = false;

        if(route.getIndex() == -1)
        {
            if((bool = routes.add(route)))
            {
                route.setJourney(this);
                route.setIndex(routes.indexOf(route));
            }
        }
        else
        {
            bool = true;
            routes.add(route.getIndex(), route);
        }
        return bool;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Journey)) return false;

        Journey journey = (Journey) o;

        return id == journey.id;

    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString()
    {
        return "Journey{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", routes=" + routes +
                '}';
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeList(routes);
    }
}
