package com.xtechnologie.radiocirculation.bll.androidservice;

import android.app.Activity;
import android.os.Bundle;

import com.xtechnologie.radiocirculation.bll.service.Services;

/**
 * Created by Alexis on 2017-05-15.
 */

public class HelperActivity extends Activity
{

    private HelperActivity ctx;

    public HelperActivity() {}


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        ctx = this;
        String action = (String) getIntent().getExtras().get("DO");
        if (action.equals("previous"))
        {
            Services.AUDIO_MANAGER.playPrevious();
        }
        else if (action.equals("play"))
        {
            Services.AUDIO_MANAGER.start();
        }
        else if (action.equals("next"))
        {
            Services.AUDIO_MANAGER.playNext();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
}
