package com.xtechnologie.radiocirculation.bll.service;

import android.provider.ContactsContract;

import com.xtechnologie.radiocirculation.dal.Repository;
import com.xtechnologie.radiocirculation.dal.RepositoryKey;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 */

public class DataService
{
    private static DataService _instance;

    private DataService(){}

    public static DataService getInstance()
    {
        if(_instance == null)
            _instance = new DataService();
        return _instance;
    }

    public void changeDAO(RepositoryKey key)
    {
        Repository.getInstance().changeIDAO(key);
    }
}
