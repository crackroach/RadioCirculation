package com.xtechnologie.radiocirculation;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;

/**
 * Created by Alexis on 2017-05-05.
 */

public class App extends Application
{
    private static App _instance;

    public static App getInstance()
    {
        return _instance;
    }

    public static Context getContext()
    {
        return _instance;
    }

    @Override
    public void onCreate()
    {
        _instance = this;
        super.onCreate();
    }


}
