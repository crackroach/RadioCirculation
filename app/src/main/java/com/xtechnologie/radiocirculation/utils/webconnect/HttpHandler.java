package com.xtechnologie.radiocirculation.utils.webconnect;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alexis on 2017-04-08.
 */

public class HttpHandler
{
    private static final String TAG = HttpHandler.class.getSimpleName();
    public static final URLRequestMethod GET = new URLRequestMethod("GET");
    public static final URLRequestMethod POST = new URLRequestMethod("POST");
    public static final int DEFAULT_TIMEOUT = 3000;
    public static final int SHORT_TIMEOUT = 1000;
    public static final int LONG_TIMEOUT = 5000;


    private int connectionTimeout;
    private List<KeyValuePair> keyValuePairList;
    private String connectionString;
    private URLRequestMethod urlRequestMethod;


    /**
     * Create a basic HttpHandler object to connect to a WebService.
     * Default timeout: 3000ms
     */
    public HttpHandler()
    {
        this(3000, "", GET);
    }

    /**
     * Create a HttpHandler object to connect to a WebService.
     *
     * The default RequestMethod is GET
     *
     * @param connectionTimeout
     */
    public HttpHandler(int connectionTimeout)
    {
        this(connectionTimeout, "", GET);
    }

    /**
     * Create a HttpHandler object to connect to a WebService.
     * The default RequestMethod is GET
     * The default ConnectionTimeout is 3000 ms
     *
     * @param connectionTimeout
     * @param connectionString
     */
    public HttpHandler(int connectionTimeout, @NonNull String connectionString)
    {
        this(connectionTimeout, connectionString, GET);
    }

    /**
     * Create a HttpHandler object to connect to a WebService.
     *
     *
     * @param connectionTimeout has to be higher than 0
     * @param connectionString
     * @param urlRequestMethod
     */
    public HttpHandler(int connectionTimeout, @NonNull String connectionString, @NonNull URLRequestMethod urlRequestMethod)
    {
        if (connectionTimeout <= 0) throw new IllegalArgumentException("The connectionTimeout has to be higher than 0");
        this.connectionTimeout = connectionTimeout;
        this.connectionString = connectionString;
        this.urlRequestMethod = urlRequestMethod;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public List<KeyValuePair> getKeyValuePairList() {
        return keyValuePairList;
    }

    public void setKeyValuePairList(List<KeyValuePair> keyValuePairList) {
        this.keyValuePairList = keyValuePairList;
    }

    public boolean addKeyValuePair(KeyValuePair kvp)
    {
        if(keyValuePairList == null)
            keyValuePairList = new ArrayList<>();
        return this.keyValuePairList.add(kvp);
    }

    public boolean addKeyValuePair(String key, String value)
    {
        return addKeyValuePair(new KeyValuePair(key, value));
    }

    public void addKeyValuePairs(KeyValuePair[] keyValuePairs)
    {
        if(keyValuePairs == null && keyValuePairs.length > 0) return;

        for(KeyValuePair kvp : keyValuePairs)
            addKeyValuePair(kvp);
    }

    public boolean removeKeyValuePair(String key)
    {
        return this.keyValuePairList.remove(key);
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public URLRequestMethod getUrlRequestMethod() {
        return urlRequestMethod;
    }

    public void setUrlRequestMethod(URLRequestMethod urlRequestMethod) {
        this.urlRequestMethod = urlRequestMethod;
    }

    /**
     * Connect to the WebService an get a string from the server. Call to execute.
     * Make sure you have provided all the information before calling this method
     *
     * @param keyValuePairs
     * @return
     */
    public String makeServiceCall(KeyValuePair... keyValuePairs)
    {
        if (connectionString.isEmpty()) throw new IllegalArgumentException("you have to provide a connectionString to your WebService.");
        for(KeyValuePair k : keyValuePairs)
            addKeyValuePair(k);
        return makeServiceCall(connectionString, urlRequestMethod, keyValuePairList);
    }

    /**
     * Connect to the WebService an get a string from the server. Call to execute.
     *
     * @param reqUrl
     * @param requestMethod
     * @param param
     * @return
     */
    public String makeServiceCall(@NonNull String reqUrl, @NonNull URLRequestMethod requestMethod, @Nullable List<KeyValuePair> param)
    {
        return makeServiceCall(reqUrl, requestMethod, param.toArray(new KeyValuePair[]{}));
    }

    /**
     * Connect to the WebService an get a string from the server. Call to execute.
     *
     * If the parameter have been specified prior to this call, local parameter will be priorize.
     * @param reqUrl
     * @param requestMethod
     * @param param will be added to existing parameter
     * @return
     */
    public String makeServiceCall(@NonNull String reqUrl, @NonNull URLRequestMethod requestMethod, @Nullable KeyValuePair... param)
    {
        this.addKeyValuePairs(param);
        this.connectionString = reqUrl;
        this.urlRequestMethod = requestMethod;

        String response = null;
        HttpResponse result = new HttpResponse(false, "", 0, "", null, this.keyValuePairList, connectionString, requestMethod.getMethod());

        try
        {
            HttpURLConnection connection;
            if(requestMethod == GET)
            {
                URL url = getQuery(reqUrl, param);
                connection = (HttpURLConnection) url.openConnection();
            }
            else if(requestMethod == POST)
            {
                URL url = new URL(reqUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("charset", "utf-8");
            }
            else
                return null;

            connection.setRequestMethod(requestMethod.getMethod());
            connection.setConnectTimeout(connectionTimeout);
            connection.connect();

            if(param.length != 0 && requestMethod == POST)
            {
                final OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(param));
                writer.flush();
                writer.close();
                os.close();
            }

            result.setHttpResponseCode(connection.getResponseCode());
            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                response = convertStreamToString(in);
                result.setSuccess(true);
                result.setResult(response);
            }
            else
            {
                result.setMessage("Connection error");
                Log.e("HttpConnection", "Connection has error");
            }
        }
        catch (MalformedURLException e)
        {
            response = null;
            result.setExceptionRaised(e);
            Log.e(TAG, "makeServiceCall: " + e.getMessage());
        }
        catch (SocketTimeoutException e)
        {
            response = null;
            result.setExceptionRaised(e);
            Log.e(TAG, "makeServiceCall: " + e.getMessage());
        }
        catch (UnsupportedEncodingException e)
        {
            response = null;
            result.setExceptionRaised(e);
            Log.e(TAG, "makeServiceCall: " + e.getMessage());
        }
        catch (IOException e)
        {
            response = null;
            result.setExceptionRaised(e);
            Log.e(TAG, "makeServiceCall: " + e.getMessage());
        }
        catch (Exception e)
        {
            response = null;
            result.setExceptionRaised(e);
            Log.e(TAG, "makeServiceCall: " + e.getMessage());
        }

        return response;
    }

    private URL getQuery(String url, KeyValuePair[] params) throws MalformedURLException, UnsupportedEncodingException
    {
        URL urlObj = null;

        StringBuilder builder = new StringBuilder();
        builder.append(url);
        builder.append('?');
        builder.append(getQuery(params));

        urlObj = new URL(builder.toString());

        return urlObj;
    }


    private String convertStreamToString(InputStream in)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder builder = new StringBuilder();

        String line;
        try
        {
            while((line = reader.readLine()) != null)
            {
                builder.append(line).append('\n');
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                in.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return builder.toString();
    }

    private static String getQuery(KeyValuePair[] keyValuePairs) throws UnsupportedEncodingException
    {
        final StringBuilder result = new StringBuilder();
        boolean first = true;
        for (KeyValuePair kvp : keyValuePairs) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(kvp.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(kvp.getValue(), "UTF-8"));
            Log.e("Key#",kvp.getKey()+"#"+kvp.getValue());
        }
        return result.toString();
    }

    private static class URLRequestMethod
    {
        private String method;

        public URLRequestMethod(String method)
        {
            this.method = method;
        }

        public String getMethod()
        {
            return method;
        }
    }
}
