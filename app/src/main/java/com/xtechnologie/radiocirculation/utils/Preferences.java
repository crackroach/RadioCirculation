package com.xtechnologie.radiocirculation.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.xtechnologie.radiocirculation.R;

import java.util.Map;
import java.util.Set;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 *
 * Classe wrapper qui sert à faire le lien entre notre application et le fichier de préférences de notre application.
 *
 * Pour avoir un objet de cette classe, il faut appeller la méthode newInstance(Context context) qui retourne un objet connecté
 * ex: Preferences pref = Preferences.newInstance(getBaseContext());
 */

public class Preferences
{
    // Nom du fichier pour la base de donnée locale
    public static final String DATABASE_NAME = "RadioCircualtion.db";

    // URLs
    public final static Urls URLS = Urls.getInstance();

    //Attributs constantes statiques pour la table Journey de la base de donnée locale
    public final static String TABLE_JOURNEYS ="journeys";
    public final static String FIELD_ID = "id";
    public final static String FIELD_NAME = "name";

    // Le context pour l'objet courant
    private Context context;

    private Preferences(Context context) {
        this.context = context;
    }

    /**
     * Retourne un objet pour communiquer avec le fichier de préférence
     * @param ctx
     * @return un nouvel objet Preferences
     */
    public static Preferences newInstance(Context ctx)
    {
        return new Preferences(ctx);
    }

    private SharedPreferences getSharedPreferences()
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getString(PreferencesKey key)
    {
        return getSharedPreferences().getString(key.getKey(), (String)key.getDefaultValue());
    }

    public void setString(PreferencesKey key, String value)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key.getKey(), value);
        editor.commit();
    }

    public int getInt(PreferencesKey key)
    {
        return getSharedPreferences().getInt(key.getKey(), (Integer)key.getDefaultValue());
    }

    public void clearAll()
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear();
        editor.commit();
    }

    public void setInt(PreferencesKey key, int value)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key.getKey(), value);
        editor.commit();
    }

    public boolean getBoolean(PreferencesKey key)
    {
        return getSharedPreferences().getBoolean(key.getKey(), (Boolean) key.getDefaultValue());
    }

    public void setBoolean(PreferencesKey key, boolean value)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key.getKey(), value);
        editor.commit();
    }

    public void removeKey(PreferencesKey key)
    {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.remove(key.getKey());
        editor.apply();
    }

    public boolean hasValue(PreferencesKey key)
    {
        return getSharedPreferences().getString(key.getKey(), null) != null;
    }
}
