package com.xtechnologie.radiocirculation.utils;

/**
 * Created by Alexis-Laptop on 2017-04-13.
 */

public class Urls
{
    private static Urls _instance;

    public final String BASE_URL = "http://xltechnologie.com/radiocirculation";
    public final String WEBSERVICE_URL = BASE_URL + "/WebService";
    public final String IMAGE_URL = BASE_URL + "/image";
    public final String AUDIO_URL = BASE_URL + "/audio";

    public final String LOGIN_URL = WEBSERVICE_URL + "/login.php";
    public final String SIGNUP_URL = WEBSERVICE_URL +  "/signup.php";
    public final String GET_SECTORS_URL = WEBSERVICE_URL + "/getSectors.php";
    public final String GET_ROUTES_BY_SECTOR = WEBSERVICE_URL + "/getRouteBySector.php";
    public final String GET_ROUTES_BY_JOURNEY = WEBSERVICE_URL + "/getRouteByJourney.php";
    public final String CREATE_JOURNEY = WEBSERVICE_URL + "/createJourney.php";
    public final String GET_JOURNEYS = WEBSERVICE_URL + "/getJourneys.php";
    public final String REMOVE_JOURNEY = WEBSERVICE_URL + "/removeJourney.php";
    public final String ADD_ROUTE = WEBSERVICE_URL + "/addRouteToJourney.php";
    public final String REMOVE_ROUTE_FROM_JOURNEY = WEBSERVICE_URL + "/removeRouteFromJourney.php";

    private Urls()
    {}

    public static Urls getInstance()
    {
        if(_instance == null)
            _instance = new Urls();
        return _instance;
    }
}
