package com.xtechnologie.radiocirculation.utils;


import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Alexis-Laptop on 2017-05-04.
 */

public class ControllableArrayList<E> extends ArrayList<E> implements ControllableList<E>
{
    private int currentItemIndex;

    public ControllableArrayList(int initialCapacity)
    {
        super(initialCapacity);
        currentItemIndex = 0;
    }

    public ControllableArrayList()
    {
        currentItemIndex = 0;
    }

    public ControllableArrayList(@NonNull Collection c)
    {
        super(c);
        currentItemIndex = 0;
    }

    @Override
    public E get(int index)
    {
        E e = super.get(index);
        currentItemIndex = index;
        return e;
    }

    public E next()
    {
        E item = null;
        if(hasNext())
        {
            currentItemIndex++;
            item = get(currentItemIndex);
        }
        return item;
    }

    public E previous()
    {
        E item = null;
        if(hasPrevious())
        {
            currentItemIndex--;
            item = get(currentItemIndex);
        }
        return item;
    }

    public boolean hasNext()
    {
        return (this.currentItemIndex + 1) < this.size();
    }

    public boolean hasPrevious()
    {
        return this.currentItemIndex != 0;
    }

    public int getNumberOfItemLeft()
    {
        return this.size() - this.currentItemIndex;
    }

    public E current()
    {
        try
        {
            return super.get(currentItemIndex);
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public void flush()
    {
        for(int i = this.size()-1; i >= 0; i--)
            this.remove(i);
        currentItemIndex = 0;
    }
}
