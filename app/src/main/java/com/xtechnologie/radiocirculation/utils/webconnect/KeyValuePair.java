package com.xtechnologie.radiocirculation.utils.webconnect;

import android.support.annotation.NonNull;

import java.util.Collection;

public class KeyValuePair<T>
{
    private String key;
    private T value;

    public KeyValuePair(@NonNull String key, T value)
    {
        if(key.isEmpty()) throw new IllegalArgumentException("key cannot be empty");
        this.key = key;
        this.value = value;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getValue()
    {
        if (value instanceof Collection)
        {
            StringBuilder builder = new StringBuilder();
            boolean first = true;

            for (Object o : (Collection) value)
            {
                if(first)
                {
                    builder.append(o.toString());
                }
                else
                {
                    builder.append(';');
                    builder.append(o.toString());
                }
            }
        }
        return value.toString();
    }

    public void setValue(T value)
    {
        this.value = value;
    }

    public T getRawValue()
    {
        return value;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof KeyValuePair)) return false;

        KeyValuePair<?> that = (KeyValuePair<?>) o;

        if (!key.equals(that.key)) return false;
        return value != null ? value.equals(that.value) : that.value == null;

    }

    @Override
    public int hashCode()
    {
        int result = key.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "KeyValuePair{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}