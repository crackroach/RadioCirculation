package com.xtechnologie.radiocirculation.utils.webconnect;


import java.util.List;

/**
 * Created by Alexis-Laptop on 2017-04-24.
 */

public class HttpResponse
{
    private boolean success;
    private String message;
    private int httpResponseCode;
    private String result;
    private Exception exceptionRaised;
    private List<KeyValuePair> keyValuePairs;
    private String connectionString;
    private String requestMethod;

    public HttpResponse(boolean success, String message, int httpResponseCode, String result, Exception exceptionRaised, List<KeyValuePair> keyValuePairs, String connectionString, String requestMethod) {
        this.success = success;
        this.message = message;
        this.httpResponseCode = httpResponseCode;
        this.result = result;
        this.exceptionRaised = exceptionRaised;
        this.keyValuePairs = keyValuePairs;
        this.connectionString = connectionString;
        this.requestMethod = requestMethod;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Exception getExceptionRaised() {
        return exceptionRaised;
    }

    public void setExceptionRaised(Exception exceptionRaised) {
        this.exceptionRaised = exceptionRaised;
    }

    public List<KeyValuePair> getKeyValuePairs() {
        return keyValuePairs;
    }

    public void setKeyValuePairs(List<KeyValuePair> keyValuePairs) {
        this.keyValuePairs = keyValuePairs;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", httpResponseCode=" + httpResponseCode +
                ", result='" + result + '\'' +
                ", exceptionRaised=" + exceptionRaised +
                ", keyValuePairs=" + keyValuePairs +
                ", connectionString='" + connectionString + '\'' +
                ", requestMethod='" + requestMethod + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HttpResponse)) return false;

        HttpResponse that = (HttpResponse) o;

        if (success != that.success) return false;
        if (httpResponseCode != that.httpResponseCode) return false;
        if (!message.equals(that.message)) return false;
        if (!result.equals(that.result)) return false;
        return exceptionRaised != null ? exceptionRaised.equals(that.exceptionRaised) : that.exceptionRaised == null;

    }

    @Override
    public int hashCode() {
        int result1 = (success ? 1 : 0);
        result1 = 31 * result1 + message.hashCode();
        result1 = 31 * result1 + httpResponseCode;
        result1 = 31 * result1 + result.hashCode();
        result1 = 31 * result1 + (exceptionRaised != null ? exceptionRaised.hashCode() : 0);
        return result1;
    }
}
