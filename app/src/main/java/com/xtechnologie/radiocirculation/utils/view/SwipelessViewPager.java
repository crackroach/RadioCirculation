package com.xtechnologie.radiocirculation.utils.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Alexis-Laptop on 2017-04-24.
 */

public class SwipelessViewPager extends ViewPager
{
    private boolean swipeEnabled;

    public SwipelessViewPager(Context context) {
        super(context);
        swipeEnabled = false;
    }

    public SwipelessViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        swipeEnabled = false;
    }

    public void setSwipeEnabled(boolean enabled)
    {
        swipeEnabled = enabled;
    }

    public boolean isSwipeEnabled()
    {
        return swipeEnabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        if (swipeEnabled)
            return super.onInterceptTouchEvent(ev);
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        if (swipeEnabled)
            return super.onTouchEvent(ev);
        return false;
    }
}
