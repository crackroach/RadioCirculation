package com.xtechnologie.radiocirculation.utils;

import java.util.List;

/**
 * Created by Alexis on 2017-05-06.
 */

public interface ControllableList<E> extends List<E>
{
    boolean hasNext();
    boolean hasPrevious();
    E next();
    E previous();
    E current();
}
