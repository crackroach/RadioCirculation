package com.xtechnologie.radiocirculation.utils;

/**
 * Created by Alexis-Laptop on 2017-04-03.
 *
 * Classe qui sert à donner accès aux informations précises de note fichier de préférence
 */

public class PreferencesKey
{
    // Clé d'accès pour le fichier de configuration
    public final static PreferencesKey IS_LOCAL_USER = new PreferencesKey("isLocalUser", false);
    //public final static PreferencesKey USERNAME = new PreferencesKey("username", null);
    //public final static PreferencesKey PASSWORD = new PreferencesKey("password", null);
    //public final static PreferencesKey EXTERNAL_DATABASE_ADRESS = new PreferencesKey("extDBPath", "http://www.xtechnologie.com:3306");
    public final static PreferencesKey REFRESH_RATE = new PreferencesKey("refreshRate", 60);
    public final static PreferencesKey LANGUAGE = new PreferencesKey("language", "fr");
    public final static PreferencesKey IS_FIRST_RUN = new PreferencesKey("isFirstRun", true);
    public final static PreferencesKey TOKEN = new PreferencesKey("token", null);
    public final static PreferencesKey USE_NOTIFICATION_PLAYER = new PreferencesKey("notificationPlayer", true);

    private static int auto_id = 1;

    private int id;
    private String key;
    private Object defaultValue;

    public PreferencesKey(String key, Object defaultValue) {
        this.id = auto_id++;
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }
}
