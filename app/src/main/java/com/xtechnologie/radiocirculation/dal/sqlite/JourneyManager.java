package com.xtechnologie.radiocirculation.dal.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.sqlite.DatabaseHelper;
import com.xtechnologie.radiocirculation.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mathieu on 2017-04-06.
 */

public class JourneyManager {
    private SQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;
    private Context context;

    public JourneyManager(Context context) {
        this.context = context;
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        init();
    }

    public void open(){
        if(!database.isOpen()){
            database = dbHelper.getWritableDatabase();
        }
    }

    //À supprimer, seulement pour test
    public void init(){
        if(Services.APP.isFirstRun(context)){
            insertJourney(new Journey(1, "Rive_sud-Rive_nord"));
            insertJourney(new Journey(2, "Pierrefonds-Montréal_Nord"));
        }
    }

    public void insertJourney(Journey journey){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Preferences.FIELD_NAME, journey.getName());
        database.insert(Preferences.TABLE_JOURNEYS, null, contentValues);
    }

    public void updateJourney(Journey journey){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Preferences.FIELD_NAME, journey.getName());

        String[] critere = new String[1];
        critere[0] = journey.getId()+"";

        database.update(Preferences.TABLE_JOURNEYS, contentValues, Preferences.FIELD_ID + "=?", critere);
    }

    public void deleteJourney(long id){
        String[] critere = new String[1];
        critere[0] = id+"";
        database.delete(Preferences.TABLE_JOURNEYS, Preferences.FIELD_ID + "=?", critere);
    }

    public void close(){
        database.close();
    }

    public List<Journey> getAllJourneys(){
        Cursor cursor =
                database.query(Preferences.TABLE_JOURNEYS, null, null, null, null, null, null);
        List<Journey> journeys = new ArrayList<>();
        if(cursor !=null)
            cursor.moveToFirst();
        do{
            Journey journey = new Journey();
            journey.setId(cursor.getInt(cursor.getColumnIndex(Preferences.FIELD_ID)));
            journey.setName(cursor.getString(cursor.getColumnIndex(Preferences.FIELD_NAME)));
            journeys.add(journey);
        }while(cursor.moveToNext());
        return journeys;
    }

    public List<Journey> getJourneyByName(String name){
        String[] critere = new String[1];
        critere[0] = name;
        Cursor cursor = database.query(Preferences.TABLE_JOURNEYS, null, Preferences.FIELD_NAME
                + "=?", critere, null, null, null);
        List<Journey> journeys = new ArrayList<>();
        if(cursor !=null)
            cursor.moveToFirst();
        do{
            Journey journey = new Journey();
            journey.setId(cursor.getInt(cursor.getColumnIndex(Preferences.FIELD_ID)));
            journey.setName(cursor.getString(cursor.getColumnIndex(Preferences.FIELD_NAME)));
            journeys.add(journey);
        }while(cursor.moveToNext());
        return journeys;
    }

    public Journey getJourneyById(long id){
        Journey journey = null;
        String[] critere = new String[1];
        critere[0] = id+"";
        Cursor cursor = database.query(Preferences.TABLE_JOURNEYS, null, Preferences.FIELD_ID
                + "=?", critere, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            journey = new Journey();
            journey.setId(cursor.getInt(cursor.getColumnIndex(Preferences.FIELD_ID)));
            journey.setName(cursor.getString(cursor.getColumnIndex(Preferences.FIELD_NAME)));
        }
        return journey;
    }
}
