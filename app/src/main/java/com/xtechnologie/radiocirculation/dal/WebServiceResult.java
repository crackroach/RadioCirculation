package com.xtechnologie.radiocirculation.dal;

import android.support.annotation.NonNull;

import com.xtechnologie.radiocirculation.R;

/**
 * Created by Alexis on 2017-04-09.
 */

public class WebServiceResult
{
    public static final WebServiceError NO_PARAMETER_PROVIDED_ERROR = new WebServiceError(1, "");
    public static final WebServiceError NO_DATA_FOUND_ERROR = new WebServiceError(2, "No data found");

    private boolean success;
    private int errno;
    private Object result;
    private String message;
    private WebServiceError error;

    public WebServiceResult(boolean success, Object result, @NonNull String message)
    {
        this(success, 0, result, message);
    }

    public WebServiceResult(boolean success, int errno, Object result, @NonNull String message)
    {
        this.success = success;
        this.errno = errno;
        this.result = result;
        this.message = message;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public int getErrno()
    {
        return errno;
    }

    public void setErrno(int errno)
    {
        this.errno = errno;
    }

    public String getErrorMessage()
    {
        String msg = "";
        switch(errno)
        {
            case 1:
                msg = "No paramters provided";
                break;
            case 2:
                msg = "No data found";
                break;
            case 3:
                msg = "Entry already exists";
                break;
            case 4:
                msg = "Request method incorrect. i.e. POST OR GET";
                break;
            case 5:
                msg = "No connection to database";
                break;
            case 6:
                msg = "No connection to WebService";
                break;
            default:
                msg = "No error or undefined";
        }

        return msg;
    }

    public Object getResult()
    {
        return result;
    }

    public <T extends Object> T getResult(Class<T> type)
    {
        return type.cast(result);
    }

    public void setResult(Object result)
    {
        this.result = result;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "WebServiceResult{" +
                "success=" + success +
                ", errno=" + errno +
                ", result=" + result +
                ", message='" + message + '\'' +
                ", errorMessage='" + getErrorMessage() +'\'' +
                '}';
    }

    private static class WebServiceError
    {
        private int id;
        private String message;

        public WebServiceError(int id, String message) {
            this.id = id;
            this.message = message;
        }

        public int getId() {
            return id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WebServiceError that = (WebServiceError) o;

            return id == that.id;

        }

        @Override
        public int hashCode() {
            return id;
        }
    }
}
