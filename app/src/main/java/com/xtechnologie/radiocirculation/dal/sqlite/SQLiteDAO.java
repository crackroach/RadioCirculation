package com.xtechnologie.radiocirculation.dal.sqlite;

import android.content.Context;

/**
 * Created by Mathieu on 2017-04-09.
 */

public class SQLiteDAO{

    private static SQLiteDAO _instance;
    JourneyManager journeyManager;

    private SQLiteDAO(Context context){
        journeyManager = new JourneyManager(context);
    }

    public static SQLiteDAO getInstance(Context context)
    {
        if(_instance == null)
            _instance = new SQLiteDAO(context);
        return _instance;
    }

    public JourneyManager getJourneyManager(){
        return this.journeyManager;
    }
}
