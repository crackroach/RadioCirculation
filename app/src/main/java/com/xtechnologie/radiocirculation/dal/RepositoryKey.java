package com.xtechnologie.radiocirculation.dal;

/**
 * Created by Alexis on 2017-03-31.
 */

public class RepositoryKey
{
    public static final RepositoryKey SQLITE = new RepositoryKey(1, "SQLITE");
    public static  final RepositoryKey MYSQL = new RepositoryKey(2, "MYSQL");

    private int id;
    private String value;

    private RepositoryKey(int id, String value)
    {
        this.id = id;
        this.value = value;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RepositoryKey that = (RepositoryKey) o;

        if (id != that.id) return false;
        return value != null ? value.equals(that.value) : that.value == null;

    }

    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return value;
    }
}
