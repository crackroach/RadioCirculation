package com.xtechnologie.radiocirculation.dal.webservice;

import android.os.AsyncTask;
import android.util.Log;

import com.xtechnologie.radiocirculation.bll.model.server.Audio;
import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.server.Sector;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;
import com.xtechnologie.radiocirculation.bll.model.web.User;
import com.xtechnologie.radiocirculation.bll.service.Services;
import com.xtechnologie.radiocirculation.dal.IDAO;
import com.xtechnologie.radiocirculation.dal.IDataServiceCaller;
import com.xtechnologie.radiocirculation.dal.WebServiceResult;
import com.xtechnologie.radiocirculation.utils.webconnect.HttpHandler;
import com.xtechnologie.radiocirculation.utils.webconnect.KeyValuePair;
import com.xtechnologie.radiocirculation.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexis on 2017-03-31.
 */

public class WebService implements IDAO
{

    @Override
    public void getUser(String email, String encryptedPassword, final IDataServiceCaller caller)
    {
        // On doit absolument étendre AsyncTask pour l'utiliser
        // La classe prends des String en entrée, rien pour marquer les updates et elle sort un User
        new AsyncTask<String, Void, WebServiceResult>()
        {
            // Cette méthode s'execute quand la tâche se termine
            // on envoie l'objet User dans notre visiteur
            @Override
            protected void onPostExecute(WebServiceResult webserviceResult)
            {
                super.onPostExecute(webserviceResult);
                caller.onTaskCompleted(webserviceResult);
            }

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            // La tâche en elle-même
            @Override
            protected WebServiceResult doInBackground(String... params)
            {
                // Je sais que je passe deux paramêtres dont j'en sort deux, email et password
                String e = params[0];
                String p = params[1];

                // Le résultat que nous allons retourner
                WebServiceResult result = null;

                // Une classe que j'ai écrite pour simplifier les traitements Http
                HttpHandler handler = new HttpHandler();
                // Le json en format String
                String rtnVal = handler.makeServiceCall(Preferences.URLS.LOGIN_URL,
                        HttpHandler.POST,
                        new KeyValuePair("email", e),
                        new KeyValuePair("password", p));
                try
                {
                    // On transforme le String en objet JSON
                    JSONObject json = new JSONObject(rtnVal);

                    // Si notre opération à réussi, nous pouvons aller chercher nos valeurs
                    if(json.getInt("success") == 1)
                    {
                        // On va chercher le Array dans le json
                        // On prend seulement le premier item (il n'est supposé n'y avoir qu'un seul item pour cette requête)
                        JSONObject obj = json.getJSONArray("users").getJSONObject(0);

                        // On crée le user à partir d'un objet json que nous sommes allé chercher précedement
                        User user = new User(obj.getInt("ID"), obj.getString("EMAIL"), obj.getString("USER_TOKEN"));
                        user.setName(obj.getString("NAME"));

                        result = new WebServiceResult(true, user, json.getString("message"));
                    }
                    else
                        result = new WebServiceResult(false, json.getInt("errno"), null, json.getString("message"));
                }
                catch (JSONException er)
                {
                    er.printStackTrace();
                }

                // On retourne notre objet à l'interne, nous sommes dans une classe interne donc la classe gère l'Autre partie de la logique.
                // À partir d'ici le thread sera fermé et la méthode onPostExecute sera appellé
                return result;
            }

            @Override
            protected void onCancelled()
            {
                super.onCancelled();
                caller.onTaskCancelled(null);
            }
        }.execute(email, encryptedPassword);
    }

    @Override
    public void getUser(String token, final IDataServiceCaller caller)
    {
        new AsyncTask<String, Integer, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult webServiceResult)
            {
                super.onPostExecute(webServiceResult);
                caller.onTaskCompleted(webServiceResult);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress();
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled();
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(String... params)
            {
                WebServiceResult result = new WebServiceResult(false, 0, null, "No called made");

                String password = params[0];

                HttpHandler handler = new HttpHandler();
                String rtnVal = handler.makeServiceCall(Preferences.URLS.LOGIN_URL,
                        HttpHandler.POST,
                        new KeyValuePair("user_token", password));

                try
                {
                    if(rtnVal == null)
                    {
                        result = new WebServiceResult(false, 6, null, "No connection");
                        this.cancel(true);
                    }

                    JSONObject json = new JSONObject(rtnVal);

                    if (json.getInt("success") == 1)
                    {
                        JSONObject obj = json.getJSONArray("users").getJSONObject(0);

                        User user = new User(obj.getInt("ID"), obj.getString("EMAIL"), obj.getString("USER_TOKEN"));
                        user.setName(obj.getString("NAME"));
                        result = new WebServiceResult(true, user, json.getString("message"));
                    }
                    else
                    {
                        result = new WebServiceResult(false, json.getInt("errno"), null, json.getString("message"));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                return result;
            }
        }.execute(token);
    }

    @Override
    public void signUp(final IDataServiceCaller caller, String email, String password, String name, int questionIndex, String answer)
    {
        new AsyncTask<String, Void, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onCancelled()
            {
                super.onCancelled();
                caller.onTaskCancelled(null);
            }

            @Override
            protected WebServiceResult doInBackground(String... params)
            {
                String e = params[0];
                String p = params[1];
                String n = params[2];
                String q = params[3];
                String a = params[4];

                WebServiceResult result = null;

                HttpHandler handler = new HttpHandler();
                String rtnVal = handler.makeServiceCall(Preferences.URLS.SIGNUP_URL,
                        HttpHandler.POST,
                        new KeyValuePair("email", e),
                        new KeyValuePair("password", p),
                        new KeyValuePair("name", n),
                        new KeyValuePair("secret_question", q),
                        new KeyValuePair("secret_answer", a));
                try
                {
                    JSONObject json = new JSONObject(rtnVal);

                    if(json.getInt("success") == 1)
                    {
                        result = new WebServiceResult(true, 0, Boolean.TRUE, json.getString("message"));
                    }
                    else
                        result = new WebServiceResult(false, json.getInt("errno"), Boolean.FALSE, json.getString("message"));
                }
                catch (JSONException e1)
                {
                    e1.printStackTrace();
                }

                return result;
            }
        }.execute(email, password, name, String.valueOf(questionIndex), answer);
    }

    @Override
    public void fetchAllSectors(final IDataServiceCaller caller)
    {
        new AsyncTask<Void, Void, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult webServiceResult)
            {
                super.onPostExecute(webServiceResult);
                caller.onTaskCompleted(webServiceResult);
            }

            @Override
            protected void onProgressUpdate(Void... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress();
            }

            @Override
            protected void onCancelled()
            {
                super.onCancelled();
                caller.onTaskCancelled(null);
            }

            @Override
            protected WebServiceResult doInBackground(Void... params)
            {
                HttpHandler handler = new HttpHandler();
                Log.i("tag", Preferences.URLS.GET_SECTORS_URL);
                String rtnVal = handler.makeServiceCall(Preferences.URLS.GET_SECTORS_URL,
                        HttpHandler.GET, new KeyValuePair[]{});

                WebServiceResult result = null;

                try
                {
                    JSONObject json = new JSONObject(rtnVal);
                    if (!rtnVal.isEmpty() && json.getInt("success") == 1)
                    {
                        List<Sector> sectors = new ArrayList<>();
                        JSONArray array = json.getJSONArray("sectors");
                        for(int i = 0, len = array.length(); i < len; i++)
                        {
                            JSONObject obj = array.getJSONObject(i);
                            Sector s = new Sector(obj.getInt("ID"), obj.getString("NAME"));
                            sectors.add(s);
                        }

                        result = new WebServiceResult(true, sectors, "");
                    }
                    else
                    {
                        result = new WebServiceResult(false, json.getInt("errno"), null, json.getString("message"));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    result = new WebServiceResult(false, 0, null, "No return value");
                }

                return result;
            }
        }.execute();
    }

    @Override
    public void getAllRoutesFromSector(final IDataServiceCaller caller, final long sectorId)
    {
        new AsyncTask<Long, Void, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult webServiceResult)
            {
                super.onPostExecute(webServiceResult);
                caller.onTaskCompleted(webServiceResult);
            }

            @Override
            protected void onProgressUpdate(Void... values)
            {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled();
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(Long... params)
            {
                WebServiceResult result = null;

                HttpHandler handler = new HttpHandler();
                String rtnVal = handler.makeServiceCall(Preferences.URLS.GET_ROUTES_BY_SECTOR,
                        HttpHandler.GET,
                        new KeyValuePair[]{ new KeyValuePair("sectorID", params[0])});

                try
                {
                    if(rtnVal == null || rtnVal.isEmpty())
                    {
                        result = new WebServiceResult(false, 6, null, "no connection");
                        this.cancel(true);
                    }
                    JSONObject json = new JSONObject(rtnVal);

                    if(!rtnVal.isEmpty() && json.getInt("success") == 1)
                    {
                        Sector sector;
                        JSONObject jsonSector = json.getJSONArray("sector").getJSONObject(0);
                        sector = new Sector(jsonSector.getLong("ID"), jsonSector.getString("NAME"));

                        JSONArray array = json.getJSONArray("routes");

                        for(int i = 0, len = array.length(); i < len; i++)
                        {
                            JSONObject o = array.getJSONObject(i);
                            Route route = new Route(o.getLong("ID"), o.getString("NAME"), o.getString("AUDIO"), o.getString("ROUTE_TYPE"));
                            sector.addRoute(route);
                        }

                        result = new WebServiceResult(true, sector, "Sector added");
                    }
                    else
                    {
                        result = new WebServiceResult(false, json.getInt("errno"), null, json.getString("message"));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                    result = new WebServiceResult(false, 0, null, "No data returned");
                }
                return result;
            }
        }.execute(sectorId);
    }

    @Override
    public void createJourney(final IDataServiceCaller caller, String journeyName, Route... route)
    {
        StringBuilder builder = new StringBuilder();
        String prefix = "";
        for(Route r : route)
        {
            builder.append(prefix);
            builder.append(r.getId());
            prefix = ",";
        }

        new AsyncTask<KeyValuePair, Integer, WebServiceResult>()
        {

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress();
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled(result);
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(KeyValuePair... params)
            {
                WebServiceResult result = new WebServiceResult(false, 0, null, "No error");

                HttpHandler handler = new HttpHandler(HttpHandler.DEFAULT_TIMEOUT,
                        Preferences.URLS.CREATE_JOURNEY,
                        HttpHandler.POST);
                handler.addKeyValuePairs(params);
                String rtnVal = handler.makeServiceCall();

                if(rtnVal != null && !rtnVal.isEmpty())
                {
                    try
                    {
                        JSONObject json = new JSONObject(rtnVal);

                        result.setSuccess(json.getInt("success") == 1);
                        result.setErrno(json.getInt("errno"));
                        result.setMessage(json.getString("message"));

                        if (json.getInt("success") == 1)
                        {
                            JSONObject jsonJourney = json.getJSONObject("journey");
                            Journey journey = new Journey(jsonJourney.getInt("ID"), jsonJourney.getString("NAME"), Services.USER_SERVICE.getCurrentUser());
                            Services.USER_SERVICE.getCurrentUser().addJourney(journey);

                            result.setResult(journey);

                        }
                        else
                            result = new WebServiceResult(false, json.getInt("errno"), null, json.getString("message"));

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        result.setMessage("Error parsing JSON");
                    }
                }
                else
                    result.setErrno(6);

                return result;
            }
        }.execute(new KeyValuePair("token", Services.USER_SERVICE.getCurrentUser().getToken()),
                  new KeyValuePair("name", journeyName),
                  new KeyValuePair("routes", builder.toString()));
    }

    @Override
    public void getJourneys(final IDataServiceCaller caller, String token)
    {
        new AsyncTask<KeyValuePair, Integer, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress(values);
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled(result);
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(KeyValuePair... params)
            {
                WebServiceResult result = new WebServiceResult(false, null, "Error unknown");

                String rtnVal = new HttpHandler(HttpHandler.DEFAULT_TIMEOUT,
                        Preferences.URLS.GET_JOURNEYS,
                        HttpHandler.GET).makeServiceCall(params[0]);
                if(rtnVal != null || !rtnVal.isEmpty())
                {
                    try
                    {
                        JSONObject json = new JSONObject(rtnVal);

                        result.setSuccess(json.getInt("success") == 1);
                        result.setMessage(json.getString("message"));
                        result.setErrno(json.getInt("errno"));

                        if (json.getInt("success") == 1)
                        {
                            List<Journey> journeys = new ArrayList<Journey>();
                            JSONArray arr = json.getJSONArray("journeys");
                            for(int i = 0, len = arr.length(); i < len; i++)
                            {
                                JSONObject o = arr.getJSONObject(i);
                                Journey j = new Journey(o.getLong("ID"), o.getString("NAME"), null);
                                j.setSize(o.getInt("NUM_ROUTE"));
                                journeys.add(j);
                            }
                            result.setResult(journeys);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        result.setMessage("String value from web service is not JSONable");
                    }
                }
                else
                    result.setMessage("String returned from webservice is either empty or null");
                return result;
            }
        }.execute(new KeyValuePair("token", token));
    }

    @Override
    public void removeJourney(final IDataServiceCaller caller, long journeyID)
    {
        new AsyncTask<KeyValuePair, Integer, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress(values);
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled(result);
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(KeyValuePair... params)
            {
                WebServiceResult result = new WebServiceResult(false, null, "Unknown");

                String rtnVal = new HttpHandler(HttpHandler.DEFAULT_TIMEOUT,
                        Preferences.URLS.REMOVE_JOURNEY,
                        HttpHandler.POST)
                        .makeServiceCall(params);

                if(rtnVal != null && rtnVal.isEmpty())
                {
                    try
                    {
                        JSONObject json = new JSONObject(rtnVal);

                        result.setSuccess(json.getInt("success") == 1);
                        result.setMessage(json.getString("message"));
                        result.setErrno(json.getInt("errno"));

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        result.setMessage("Error parsing JSON");
                    }

                }
                else
                    result.setMessage("No data returned from web service");

                return result;
            }
        }.execute(new KeyValuePair("journeyID", journeyID));
    }

    @Override
    public void addRouteToJourney(final IDataServiceCaller caller, long journeyID, Route... route)
    {
        StringBuilder builder = new StringBuilder();
        String prefix = "";
        for(Route r : route)
        {
            builder.append(prefix);
            builder.append(r.getId());
            prefix = ",";
        }

        new AsyncTask<KeyValuePair, Integer, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress(values);
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled(result);
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(KeyValuePair... params)
            {
                WebServiceResult result = new WebServiceResult(false, 0, null, "Error unknown");

                String rtnVal = new HttpHandler().makeServiceCall(Preferences.URLS.ADD_ROUTE,
                        HttpHandler.POST,
                        params);
                try
                {
                    if(rtnVal != null && !rtnVal.isEmpty())
                    {
                        JSONObject json = new JSONObject(rtnVal);

                        result.setSuccess(json.getInt("success") == 1);
                        result.setMessage(json.getString("message"));
                        result.setErrno(json.getInt("errno"));
                    }
                    else
                        result.setMessage("No return value");
                }
                catch(JSONException e)
                {
                    result.setMessage(e.getMessage());
                    result.setErrno(0);
                }

                return result;
            }
        }.execute(new KeyValuePair("journeyID", journeyID), new KeyValuePair("routesID", builder.toString()));
    }

    @Override
    public void getAllRouteFromJourney(final IDataServiceCaller caller, long journeyId)
    {
        new AsyncTask<KeyValuePair, Integer, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress(values);
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled(result);
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(KeyValuePair... params)
            {
                WebServiceResult result = new WebServiceResult(false, 0, null, "No error");

                String rtnVal = new HttpHandler(HttpHandler.DEFAULT_TIMEOUT,
                        Preferences.URLS.GET_ROUTES_BY_JOURNEY,
                        HttpHandler.GET)
                        .makeServiceCall(params);
                if(rtnVal != null && !rtnVal.isEmpty())
                {
                    try
                    {
                        JSONObject json = new JSONObject(rtnVal);

                        result.setErrno(json.getInt("errno"));
                        result.setMessage(json.getString("message"));
                        result.setSuccess(json.getInt("success") == 1);

                        if(result.isSuccess())
                        {
                            JSONObject jsonJourney = json.getJSONObject("journey");
                            Journey journey = new Journey(jsonJourney.getLong("ID"), jsonJourney.getString("NAME"));

                            JSONArray arr = jsonJourney.getJSONArray("routes");

                            for(int i = 0; i < arr.length(); i++)
                            {
                                JSONObject jsonRoute = arr.getJSONObject(i);
                                JSONObject jsonSector = jsonRoute.getJSONObject("sector");
                                JSONObject jsonAudio = jsonRoute.getJSONObject("audio");

                                journey.addRoute(new Route(jsonRoute.getLong("ID"),
                                        jsonRoute.getString("NAME"),
                                        new Sector(jsonSector.getLong("ID"), jsonSector.getString("NAME")),
                                        new Audio(jsonAudio.getLong("ID"), jsonAudio.getString("FILEPATH"), null),
                                        null));
                            }

                            result.setResult(journey);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        result.setMessage("Error parsing JSON result");
                        result.setResult(e);
                    }
                }
                else
                {
                    result.setErrno(6);
                    result.setMessage("No connection to webservice");
                }

                return result;
            }
        }.execute(new KeyValuePair("journeyID", journeyId));
    }

    @Override
    public void removeRouteFromJourney(final IDataServiceCaller caller, long journeyID, long routeID)
    {
        new AsyncTask<KeyValuePair, Integer, WebServiceResult>()
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                caller.onPreExecute(null);
            }

            @Override
            protected void onPostExecute(WebServiceResult result)
            {
                super.onPostExecute(result);
                caller.onTaskCompleted(result);
            }

            @Override
            protected void onProgressUpdate(Integer... values)
            {
                super.onProgressUpdate(values);
                caller.onProgress(values);
            }

            @Override
            protected void onCancelled(WebServiceResult result)
            {
                super.onCancelled(result);
                caller.onTaskCancelled(result);
            }

            @Override
            protected WebServiceResult doInBackground(KeyValuePair... params)
            {
                WebServiceResult result = new WebServiceResult(false, 0, null, "No error");

                String rtnVal = new HttpHandler(HttpHandler.DEFAULT_TIMEOUT,
                        Preferences.URLS.REMOVE_ROUTE_FROM_JOURNEY,
                        HttpHandler.POST).makeServiceCall(params);

                if(rtnVal != null && !rtnVal.isEmpty())
                {
                    try
                    {
                        JSONObject json = new JSONObject(rtnVal);

                        result.setSuccess(json.getInt("success") == 1);
                        result.setMessage(json.getString("message"));
                        result.setErrno(json.getInt("errno"));
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                        result.setMessage("Cannot parse JSON");
                    }
                }
                else
                    result.setMessage("No data returned");

                return result;
            }
        }.execute();
    }
}
