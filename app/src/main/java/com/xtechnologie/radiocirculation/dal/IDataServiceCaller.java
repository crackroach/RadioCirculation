package com.xtechnologie.radiocirculation.dal;

/**
 * Created by Alexis on 2017-04-08.
 */

public interface IDataServiceCaller
{
    void onTaskCompleted(WebServiceResult result);
    void onProgress(Integer... value);
    void onPreExecute(Object obj);
    void onTaskCancelled(WebServiceResult result);
}
