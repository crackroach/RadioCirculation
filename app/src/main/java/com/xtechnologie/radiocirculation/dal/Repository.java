package com.xtechnologie.radiocirculation.dal;

import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.dal.webservice.WebService;

import java.util.HashMap;

/**
 * Created by Alexis on 2017-03-31.
 */

public class Repository implements IDAO
{
    private static Repository _instance;
    private HashMap<RepositoryKey, IDAO> idaos;
    private IDAO currentDAO;

    private Repository()
    {
        idaos = new HashMap<>();
        idaos.put(RepositoryKey.MYSQL, new WebService());
        //idaos.put(RepositoryKey.SQLITE, new SQLiteDAO());
        currentDAO = idaos.get(RepositoryKey.MYSQL);
    }

    public static Repository getInstance()
    {
        if (_instance == null)
            _instance = new Repository();
        return _instance;
    }

    public void changeIDAO(RepositoryKey key)
    {
        currentDAO = idaos.get(key);
    }

    @Override
    public void getUser(String email, String encryptedPassword, IDataServiceCaller caller)
    {
        currentDAO.getUser(email, encryptedPassword, caller);
    }

    @Override
    public void getUser(String token, IDataServiceCaller caller)
    {
        currentDAO.getUser(token, caller);
    }

    @Override
    public void signUp(IDataServiceCaller caller, String email, String encryptedPassword, String name, int questionIndex, String answer)
    {
        currentDAO.signUp(caller, email, encryptedPassword, name, questionIndex, answer);
    }

    @Override
    public void fetchAllSectors(IDataServiceCaller caller)
    {
        currentDAO.fetchAllSectors(caller);
    }

    @Override
    public void getAllRoutesFromSector(IDataServiceCaller caller, long sectorId)
    {
        currentDAO.getAllRoutesFromSector(caller, sectorId);
    }

    @Override
    public void createJourney(IDataServiceCaller caller, String journeyName, Route... routes)
    {
        currentDAO.createJourney(caller, journeyName, routes);
    }

    @Override
    public void addRouteToJourney(IDataServiceCaller caller, long journeyID, Route... route)
    {
        currentDAO.addRouteToJourney(caller, journeyID, route);
    }

    @Override
    public void getJourneys(IDataServiceCaller caller, String token)
    {
        currentDAO.getJourneys(caller, token);
    }

    @Override
    public void removeJourney(IDataServiceCaller caller, long journeyID)
    {
        currentDAO.removeJourney(caller, journeyID);
    }

    @Override
    public void getAllRouteFromJourney(IDataServiceCaller caller, long journeyId)
    {
        currentDAO.getAllRouteFromJourney(caller, journeyId);
    }

    @Override
    public void removeRouteFromJourney(IDataServiceCaller caller, long journeyID, long routeID)
    {
        currentDAO.removeRouteFromJourney(caller, journeyID, routeID);
    }
}
