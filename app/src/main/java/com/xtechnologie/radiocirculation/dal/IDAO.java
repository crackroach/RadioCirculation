package com.xtechnologie.radiocirculation.dal;

import com.xtechnologie.radiocirculation.bll.model.server.Route;
import com.xtechnologie.radiocirculation.bll.model.web.Journey;

/**
 * Created by Alexis on 2017-03-31.
 */

public interface IDAO
{
    // TODO: Décommenter les méthodes. Je les ai mis en commentaire pour ne pas avoir d'erreur

    void getUser(String email, String encryptedPassword, IDataServiceCaller caller);
    void getUser(String token, IDataServiceCaller caller);
    void signUp(IDataServiceCaller caller, String email, String encryptedPassword, String name, int questionIndex, String answer);
    void fetchAllSectors(IDataServiceCaller caller);
    // void changeUserPassword(IDataServiceCaller caller, String currentToken, String oldPassword, String newPassword);
    void getJourneys(IDataServiceCaller caller, String token);
    void getAllRouteFromJourney(IDataServiceCaller caller, long journeyId);
    void getAllRoutesFromSector(IDataServiceCaller caller, long sectorId);
    void removeRouteFromJourney(IDataServiceCaller caller, long journeyID, long routeID);
//    void createJourney(IDataServiceCaller, Journey journey);
    void createJourney(IDataServiceCaller caller, String journeyName, Route... route);
    void addRouteToJourney(IDataServiceCaller caller, long journeyID, Route... route);
    void removeJourney(IDataServiceCaller caller, long journeyID);

}
