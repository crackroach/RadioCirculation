package com.xtechnologie.radiocirculation.dal.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.xtechnologie.radiocirculation.utils.Preferences;

/**
 * Created by Mathieu on 2017-04-06.
 */

public class DatabaseHelper extends SQLiteOpenHelper{


    private final static String TAG = "RadioCirculationSQLiteHelper";
    public static  String JOURNEY_SCHEMA;

    public DatabaseHelper(Context context) {
        super(context, Preferences.DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        JOURNEY_SCHEMA = "CREATE TABLE " + Preferences.TABLE_JOURNEYS + " (" +
                Preferences.FIELD_ID + " integer primary key autoincrement," +
                Preferences.FIELD_NAME + " text NULL DEFAULT NULL" +
                ");";
        db.execSQL(JOURNEY_SCHEMA);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO:Transférer le data de la vieille base à la nouvelle
        String drop_Contact = "drop table " + Preferences.TABLE_JOURNEYS;
        db.execSQL(drop_Contact);
        onCreate(db);
    }
}
